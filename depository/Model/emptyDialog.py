# -*- coding: utf-8 -*-
'''
@Date: 2020-04-26 00:07:56
@LastEditors: figo
@LastEditTime: 2020-04-26 01:48:21
@FilePath: \gitLab\sstoolshelf\debug\depository\Model\emptyDialog.py
'''
try:
    import maya.cmds as cmds
    import maya.mel as mel
    import maya.OpenMayaUI as omui
    import maya.OpenMaya as om
    import pymel.core.datatypes as dt
    def getMayaWindow():
        ptr = omui.MQtUtil.mainWindow()
        return wrapInstance(long(ptr), qw.QWidget)

    MayaParent = getMayaWindow()
    # maya相关模块加载
except:
    MayaParent = None

from Qt import QtWidgets as qw,QtCore,QtGui,IsPySide,IsPySide2
# 工具架启动的时候会自动载入Qt模块来适配各种Qt版本
# 如需移植Qt模块，请查看Qt.__file__

if IsPySide:
    from shiboken import wrapInstance
elif IsPySide2:
    from shiboken2 import wrapInstance

import sys
import logging

logging.basicConfig(level = logging.INFO,format = '%(levelname)s|%(asctime)s - %(name)s - %(funcName)s : %(message)s')
logger = logging.getLogger('emptyDialog')
logger.setLevel(logging.DEBUG)

class emptyDialog(qw.QMainWindow):

    @property
    def title(self):
        return self._title

    @title.setter
    def title(self,val):
        if isinstance(val,str):
            self._title = val
            self.setWindowTitle(val)

    @property
    def x(self):
        return self._x

    @x.setter
    def x(self,val):
        if isinstance(val,int):
            self._x = val
            self.resize(val,self.y)
    @property
    def y(self):
        return self._y

    @y.setter
    def y(self,val):
        if isinstance(val,int):
            self._y = val
            self.resize(self.x,val)


    def __init__(self,title='default figo widget',parent=None):
        #super(emptyDialog,self).__init__(parent)
        qw.QMainWindow.__init__(self)
        # 2.7和3.0的super写法与工具架不兼容，请使用2.2写法。


        self._title='' 
        self._x=0 
        self._y=0
        self.m_DragPosition=self.pos()
        self.m_drag=False
        # 预设变量

        self.x = 400
        self.y = 300
        # 设置默认窗口大小
        self.move(0,0)
        # 打开窗口后，窗口默认的位置

        self.title = title
        self.initData()
        self.readSettings()
        # 读取窗口设定

        self.setWindowFlags(QtCore.Qt.SubWindow|QtCore.Qt.FramelessWindowHint | QtCore.Qt.WindowStaysOnTopHint)
        # 子窗口，无边框，永远在最上

        self.setMouseTracking(True)
        # 鼠标追踪，适用于无标题栏拖拽


        self.createUI()

    def createSubTitleLine(self,height=25):
        rtLine = qw.QHBoxLayout()
        Xbtn=qw.QPushButton('x')
        Xbtn.setFixedHeight(height)
        Xbtn.setFixedWidth(height)
        rtLine.addWidget(Xbtn)
        rtLine.addStretch()
        
        
        #-----TitleLabel-----------------------
        TitleLab=qw.QLabel('create joint base on curve')
        TitleLab.setFixedHeight(height)
        rtLine.addWidget(TitleLab)
        rtLine.addStretch()

        Xbtn.clicked.connect(self.Cl_Ui)
        return rtLine

    def createUI(self):
        mian=qw.QWidget(self)
        self.setCentralWidget(mian)
        self.mv = qw.QVBoxLayout()
        mian.setLayout(self.mv)
        # 主框架，设置主要在self.mv上
                                               
        titleLine = self.createSubTitleLine()
        self.mv.addLayout( titleLine )
        # 伪标题栏

        #------main btn-----------------------


        
        self.BtnA1=qw.QPushButton('push it')
        self.BtnA1.setEnabled(True)
        self.BtnA1.setFixedHeight(20)
        self.BtnA1.setFixedWidth(60)
        # button的一些设定

        self.mv.addWidget(self.BtnA1)
        # 将btn添加到layout中  

        self.BtnA1.clicked.connect(self.BtnFn)
        # 给button绑定一个功能，新式写法
        

    # =====================================================================================
    # 以下内容是模板内容，请参考注释酌情修改

    def initData(self):
        self.dataSettings = QtCore.QSettings("SStoolshelf - subwindows", "emptyDialog")
        # 创建和读取qsetting的设置
        # 前一项是组，后一个是项，为了避免串数据，请单独设置组和项的名字。

    def readSettings(self):
        self.dataSettings.beginGroup('SStoolshelf - subwindows - main')
        windowGeometry = self.dataSettings.value('window_geometry')
        windowState = self.dataSettings.value('window_state')
        self.restoreGeometry(windowGeometry)
        self.restoreState(windowState)
        self.dataSettings.endGroup()

    def writeSettings(self):
        self.dataSettings.beginGroup('SStoolshelf - subwindows - main')
        self.dataSettings.setValue('window_geometry', self.saveGeometry())
        self.dataSettings.setValue('window_state', self.saveState())
        self.dataSettings.endGroup()

        
    def mousePressEvent(self,event):
        if event.button() == QtCore.Qt.LeftButton or event.button() == QtCore.Qt.RightButton:
            self.m_drag=True
            self.m_DragPosition=event.globalPos()-self.pos()
            event.accept()
            
    def mouseMoveEvent(self,QMouseEvent):
        if QMouseEvent.buttons() and QtCore.Qt.LeftButton:
            self.move(QMouseEvent.globalPos()-self.m_DragPosition)
            QMouseEvent.accept()
            
    def mouseReleaseEvent(self,QMouseEvent):
        self.m_drag=False

    def closeEvent(self, event):
        self.writeSettings()
        # 写入临时信息，比如位置大小等
        self.deleteLater()  
        # 在窗口关闭的时候触发销毁自己的行为

    # 以上内容是模板内容，请参考注释酌情修改
    # =====================================================================================

    def BtnFn(self):
        logger.info('press button')
       
    def Op_Ui(self):
        # 显示接口
        self.show()
        
    def Cl_Ui(self):
        # 关闭接口
        self.close()
global titleVar
titleVar = emptyDialog()
titleVar.Op_Ui()