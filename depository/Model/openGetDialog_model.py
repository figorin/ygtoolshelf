'''
@Date: 2020-04-25 22:29:07
LastEditors: figo - uz
LastEditTime: 2020-12-24 14:32:03
FilePath: \undefinedc:\gitlab\sstoolshelf\debug\FgToolshelf\depository\Model\openGetDialog_model.py
'''
import sys
import os

try:
    toolPath = os.environ['FGPIPEROOT']
except KeyError:
    # toolPath = r'S:\PipeProgram\figoPipe\pipelineMainProgram'
    toolPath = r'C:\gitLab\brownser01'
finally:
    if toolPath not in sys.path:
        sys.path.append(toolPath)

import figoBrownser.platforms.maya.model.ModelSetting_Model_Get as model_get
reload(model_get)

global FGbrownserWindow
FGbrownserWindow = None
FGbrownserWindow = model_get.main()