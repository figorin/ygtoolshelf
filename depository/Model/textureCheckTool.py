# -*- coding: utf-8 -*-
'''
@Date: 2020-04-26 00:07:56
LastEditors: figo - uz
LastEditTime: 2020-11-26 16:47:55
FilePath: \gitlab\sstoolshelf\debug\FgToolshelf\depository\Model\textureCheckTool.py
'''
try:
    import maya.cmds as cmds
    import maya.mel as mel
    import maya.OpenMayaUI as omui
    import maya.OpenMaya as om
    import pymel.core.datatypes as dt
    def getMayaWindow():
        ptr = omui.MQtUtil.mainWindow()
        return wrapInstance(long(ptr), qw.QWidget)

    MayaParent = getMayaWindow()
    # maya相关模块加载
except:
    MayaParent = None

from Qt import QtWidgets as qw,QtCore,QtGui,IsPySide,IsPySide2
# 工具架启动的时候会自动载入Qt模块来适配各种Qt版本
# 如需移植Qt模块，请查看Qt.__file__

if IsPySide:
    from shiboken import wrapInstance
elif IsPySide2:
    from shiboken2 import wrapInstance

import sys
import logging
import json
import os
import shutil

logging.basicConfig(level = logging.INFO,format = '%(levelname)s|%(asctime)s - %(name)s - %(funcName)s : %(message)s')
logger = logging.getLogger('textureRenameDialog')
logger.setLevel(logging.DEBUG)




class textureRenameDialog(qw.QMainWindow):


    def getSource_file(self):
        gets = cmds.ls(type='file')
        filePaths = {}
        for i in gets:
            getAttr = cmds.getAttr('%s.fileTextureName' % i)
            if getAttr != '':
                filePaths[i] = cmds.getAttr('%s.fileTextureName' % i)

        return filePaths
            


    def __init__(self,title=u'贴图命名统一工具_我的三体',parent=None):
        #super(ModelRestructDialog,self).__init__(parent)
        qw.QMainWindow.__init__(self)
        # 2.7和3.0的super写法与工具架不兼容，请使用2.2写法。


        self._title='' 
        self._x=0 
        self._y=0
        self.m_DragPosition=self.pos()
        self.m_drag=False
        # 预设变量

        self.x = 400
        self.y = 300
        # 设置默认窗口大小
        self.move(0,0)
        # 打开窗口后，窗口默认的位置

        self.title = title
        self.initData()
        self.readSettings()
        # 读取窗口设定

        # self.setWindowFlags(QtCore.Qt.SubWindow|QtCore.Qt.FramelessWindowHint | QtCore.Qt.WindowStaysOnTopHint)
        # 子窗口，无边框，永远在最上

        self.setMouseTracking(True)
        # 鼠标追踪，适用于无标题栏拖拽


        self.comboList = [u'dif - 漫反',
            u'spc - 高光',
            u'nor - 法线',
            u'rgh - 粗糙',
            u'ref - 反射',
            u'msk - 遮罩',
            u'dis - 置换',
            u'mtl - 金属度',
            u'bmp - 凹凸',
            u'rfa - 折射',
            u'sss - 次表面',
            u'occ - 环境阻塞']
        self.nameCollect = []
        # 自动填充角色名功能使用的收集角色名的组，会把出现次数最多的元素当做正式名


        self.createUI()




    def createSubTitleLine(self,height=25):
        rtLine = qw.QHBoxLayout()
        Xbtn=qw.QPushButton('x')
        Xbtn.setFixedHeight(height)
        Xbtn.setFixedWidth(height)
        rtLine.addWidget(Xbtn)
        rtLine.addStretch()
        
        
        #-----TitleLabel-----------------------
        TitleLab=qw.QLabel(u'我三S4贴图整理工具')
        TitleLab.setFixedHeight(height)
        rtLine.addWidget(TitleLab)
        rtLine.addStretch()

        Xbtn.clicked.connect(self.Cl_Ui)
        return rtLine


    def createUI(self):
        mian=qw.QWidget(self)
        self.setCentralWidget(mian)
        self.mv = qw.QVBoxLayout()
        mian.setLayout(self.mv)
        # 主框架，设置主要在self.mv上
                                               
        titleLine = self.createSubTitleLine()
        self.mv.addLayout( titleLine )
        # 伪标题栏


        chaline = qw.QHBoxLayout()
        scLab = qw.QLabel()
        scLab.setText('角色名 : ')
        chaline.addWidget(scLab)
        self.chaEdit = qw.QLineEdit()
        self.chaEdit.textChanged.connect(self.refreshAllFinal)
        chaline.addWidget(self.chaEdit)
        self.mv.addLayout(chaline)

        self.mainTable = qw.QTableWidget(0,5)
        self.mv.addWidget(self.mainTable)

        labels = [u'原名',u'部位名',u'功能名',u'最终名称',u'功能']

        self.mainTable.setHorizontalHeaderLabels(labels)
        #print type(self.mainTable.horizontalHeader())
        self.mainTable.horizontalHeader().setSectionResizeMode(0, qw.QHeaderView.Stretch)
        self.mainTable.horizontalHeader().setSectionResizeMode(1, qw.QHeaderView.Stretch)
        self.mainTable.horizontalHeader().setSectionResizeMode(3, qw.QHeaderView.Stretch)

        self.mainTable.horizontalHeader().setSectionResizeMode(2, qw.QHeaderView.Fixed)
        self.mainTable.horizontalHeader().resizeSection(2,100)
        self.mainTable.horizontalHeader().setSectionResizeMode(4, qw.QHeaderView.Fixed)
        self.mainTable.horizontalHeader().resizeSection(4,60)

        self.nameCollect = []
        self.loadData()

        if self.nameCollect:
            dic = {}
            maxCount = 0
            maxItem = ''
            for i in list(set(self.nameCollect)):
                dic[i] = self.nameCollect.count(i)
                if dic[i]>maxCount:
                    maxItem = i
            self.chaEdit.setText(maxItem)

    def loadData(self):
        fileData = self.getSource_file()

        inputDict = {}
        for key,item in fileData.items():
            if item in inputDict.keys():
                inputDict[item].append(key)
            else:
                inputDict[item] = [key]

        count = 0
        for key,item in inputDict.items():
            shortName = os.path.basename(key)
            self.addRow(shortName,count,nodeArray = item)
            count+=1


    def addRow(self,mark,row,nodeArray=[]):
        
        self.mainTable.setRowCount(row+1)
        strRow =  str(row)
        
        nameitem = qw.QTableWidgetItem ()
        nameitem.setText(mark)

        self.mainTable.setItem(row,0,nameitem)

        partitem = qw.QLineEdit ()
        partitem.setText('')
        partitem.setToolTip(strRow)
        # partitem.setProperty('row',row)

        self.mainTable.setCellWidget(row,1,partitem)

        partitem.textChanged.connect(self.refreshFinalFn)



        cataComb = qw.QComboBox()
        cataComb.addItems(self.comboList)
        cataComb.setToolTip(strRow)
        self.mainTable.setCellWidget(row,2,cataComb)

        cataComb.currentTextChanged.connect(self.refreshFinalFn)

        finalitem = qw.QTableWidgetItem ()
        finalitem.setText('')

        self.mainTable.setItem(row,3,finalitem)


        funBtn = qw.QPushButton()
        funBtn.setText('rename')
        funBtn.setToolTip(strRow)
        funBtn.setProperty('row',row)
        funBtn.setProperty('node',json.dumps(nodeArray))
        self.mainTable.setCellWidget(row,4,funBtn)

        funBtn.clicked.connect(self.renameFn)
        self.refreshFinalFn(getstr='',row=row)


    def refreshAllFinal(self):
        theCount = self.mainTable.rowCount()
        for i in range(theCount):
            self.refreshFinalFn(getstr='',row=i)

    def refreshFinalFn(self,getstr='',row = -1):
        # 角色名_部位名_类型_udim.文件类型
        # 通过row控制的，来刷新final名的功能
        if row == -1:
            row = int(self.sender().toolTip())


        nameItem = self.chaEdit
        origItem = self.mainTable.item(row,0)
        partItem = self.mainTable.cellWidget(row,1)
        typeItem = self.mainTable.cellWidget(row,2)
        finalItem = self.mainTable.item(row,3)

        nameData = nameItem.text().lower()
        # print 'origItem',origItem.text()
        partData = partItem.text().lower()
        typeKey = typeItem.currentText().split(' ')[0]
        # print 'finalItem',finalItem.text()

        udimKey = ''
        if '<udim>' in origItem.text().lower():
            print '%s is udim texture'%origItem.text()
            udimKey = '_<udim>'

        spOgDot = origItem.text().split('.')
        spOgDash = spOgDot[0].split('_')

        if partData == '' and len(spOgDash) >2:
            partData = spOgDash[1]
            partItem.setText(partData)

        if nameData == '' and len(spOgDash) >2:
            nameData = spOgDash[0].lower()
            self.nameCollect.append(nameData)
        


        comboIndex = 0
        if len(spOgDash) >2:
            for count,i in enumerate(self.comboList):
                if spOgDash[2] in i:
                    typeItem.setCurrentIndex(count)
                    typeKey = typeItem.currentText().split(' ')[0]
        dt = {
            'cha':nameData.lower(),
            'part':partData.lower(),
            'type':typeKey,
            'filetype':spOgDot[-1],
            'udim':udimKey

        }
        pt = '%(cha)s_%(part)s_%(type)s%(udim)s.%(filetype)s'
        finalItem.setText(pt % dt)

        if finalItem.text() != origItem.text():
            finalItem.setBackground(QtGui.QBrush(QtGui.QColor(88,40,40)))
        else:
            finalItem.setBackground(QtGui.QBrush(QtGui.QColor(45,45,45)))


    def renameFn(self):
        # rename功能的按钮上有row和node两个属性，储存了行数和涉及到的节点名
        # 在节点中循环，把原名改成row行的final名
        # 如果节点的fileTextureName和计算出来的newPath一致，就不改文件本身了
        # psd文件，和不存在的文件，会提示报错
        sender = self.sender()
        data = json.loads(sender.property('node'))
        row = int(sender.property('row'))
        newName = self.mainTable.item(row,3).text()
        for i in data:
            path = cmds.getAttr('%s.fileTextureName' % i)
            spPath = path.split('.')
            if spPath[-1].lower()=='psd':
                cmds.select(i)
                cmds.confirmDialog(m=u'psd文件%s禁止上传,无法改名,错误节点已选中'%os.path.basename(path))
                return False
            if os.path.exists(path):
                theDir = os.path.dirname( path)
                newPath = '/'.join([theDir,newName])
                if path != newPath:
                    shutil.copy2(path,newPath)
                cmds.setAttr('%s.fileTextureName' % i,newPath,type='string')
            else:
                cmds.select(i)
                cmds.confirmDialog(m=u'节点%s中的贴图不存在,错误节点已选中'%i)
                break

        self.loadData()
    # =====================================================================================
    # 以下内容是模板内容，请参考注释酌情修改

    @property
    def title(self):
        return self._title

    @title.setter
    def title(self,val):
        if isinstance(val,str):
            self._title = val
            self.setWindowTitle(val)

    @property
    def x(self):
        return self._x

    @x.setter
    def x(self,val):
        if isinstance(val,int):
            self._x = val
            self.resize(val,self.y)
    @property
    def y(self):
        return self._y

    @y.setter
    def y(self,val):
        if isinstance(val,int):
            self._y = val
            self.resize(self.x,val)

    def initData(self):
        self.dataSettings = QtCore.QSettings("SStoolshelf - subwindows", "ModelRestructDialog")
        # 创建和读取qsetting的设置
        # 前一项是组，后一个是项，为了避免串数据，请单独设置组和项的名字。

    def readSettings(self):
        self.dataSettings.beginGroup('SStoolshelf - ModelRestructDialog - main')
        windowGeometry = self.dataSettings.value('window_geometry')
        windowState = self.dataSettings.value('window_state')
        self.restoreGeometry(windowGeometry)
        self.restoreState(windowState)
        self.dataSettings.endGroup()

    def writeSettings(self):
        self.dataSettings.beginGroup('SStoolshelf - ModelRestructDialog - main')
        self.dataSettings.setValue('window_geometry', self.saveGeometry())
        self.dataSettings.setValue('window_state', self.saveState())
        self.dataSettings.endGroup()

        
    def mousePressEvent(self,event):

        if event.button() == QtCore.Qt.LeftButton or event.button() == QtCore.Qt.RightButton:
            self.m_drag=True
            self.m_DragPosition=event.globalPos()-self.pos()
            event.accept()
            
    def mouseMoveEvent(self,QMouseEvent):

        if QMouseEvent.buttons() and QtCore.Qt.LeftButton:
            self.move(QMouseEvent.globalPos()-self.m_DragPosition)
            QMouseEvent.accept()
            
    def mouseReleaseEvent(self,QMouseEvent):
        self.m_drag=False

    def closeEvent(self, event):
        self.writeSettings()
        # 写入临时信息，比如位置大小等
        self.deleteLater()  
        # 在窗口关闭的时候触发销毁自己的行为

    def Op_Ui(self):
        # 显示接口
        self.show()
        
    def Cl_Ui(self):
        # 关闭接口
        self.close()
    # 以上内容是模板内容，请参考注释酌情修改
    # =====================================================================================


titleVar = textureRenameDialog()
titleVar.Op_Ui()

# itemname = 'chengxin'
# itemcata = 'character'
# itemvari = 'default'
# itemvers = 1


                            
#createbasic()
    
    
    
    
    
    
    
    