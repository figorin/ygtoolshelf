'''
@Date: 2020-04-25 22:29:07
LastEditors: figo-ds
LastEditTime: 2020-12-24 16:15:59
@FilePath: \gitLab\sstoolshelf\debug\depository\Model\openPublishDialog.py
'''
import sys
import os
try:
    toolPath = os.environ['FGPIPEROOT']
except KeyError:
    # toolPath = r'S:\PipeProgram\figoPipe\pipelineMainProgram'
    toolPath = r'C:\gitLab\brownser01'
finally:
    if toolPath not in sys.path:
        sys.path.append(toolPath)
        
import figoBrownser.platforms.maya.model.ModelSetting_ENV_Publish as env_pub
reload(env_pub)

global FGbrownserWindow
FGbrownserWindow = None
FGbrownserWindow = env_pub.main()