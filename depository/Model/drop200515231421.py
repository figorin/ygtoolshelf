# -*- coding: utf-8 -*-
import maya.cmds as cmds
import os


version = cmds.about(v=True)
thePath = '%s/Documents/maya/%s/modules' % (os.environ['userprofile'],version[:4])

if not os.path.exists(thePath):
    os.makedirs(thePath)

theFile = thePath + '/spore.mod'
if os.path.exists(theFile):
    try:
        if not cmds.pluginInfo('spore',query=True,loaded=True):
            cmds.loadPlugin('spore')
    except:
        cmds.error('before use spore.please restart maya')
    import sys;sys._global_spore_dispatcher.spore_manager.show()
else:
    # 安装工具本身提供的mod
    modString = ''
    modString += '+ spore any %s/depository/Model/spore-master\n' % os.environ['sstoolshelfpath']
    modString += '[r] scripts: scripts'
    
    with open(theFile,'w') as f:
        f.write(modString)
    cmds.error('please restart maya to active spore')
    


