'''
@Date: 2020-04-25 22:29:07
LastEditors: figo-ds
LastEditTime: 2020-12-24 16:16:06
FilePath: \undefinedc:\gitlab\sstoolshelf\debug\FgToolshelf\depository\Model\openPublishDialog.py
'''
import sys
import os
try:
    toolPath = os.environ['FGPIPEROOT']
except KeyError:
    # toolPath = r'S:\PipeProgram\figoPipe\pipelineMainProgram'
    toolPath = r'C:\gitLab\brownser01'
finally:
    if toolPath not in sys.path:
        sys.path.append(toolPath)

import figoBrownser.platforms.maya.model.ModelSetting_Model_Publish as bronser
reload(bronser)

global FGbrownserWindow
FGbrownserWindow = None
FGbrownserWindow = bronser.main()