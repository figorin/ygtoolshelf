#
# remove namespaces
#
# uses selected objects and only removes top namespace
import pymel.core as pm

def remove_namespaces() :
    # gather all namespaces from selection
    all_ns = []
    for obj in pm.selected() :
        if obj.namespace() :
            all_ns.append(obj.namespace())
    
    # remove dupes
    all_ns = list(set(all_ns)) 
    
    # try to remove the first namespace
    for whole_ns in all_ns :
        ns = whole_ns.split(':')[0]
        try :
            pm.namespace(mv=[ns,':'],f=1)
            if ns in pm.namespaceInfo(lon=1) :
                pm.namespace(rm=ns)
            print 'Namespace "%s" removed.'%ns
        except :
            warning('Namespace "%s" is not removable. Possibly from a reference.'%ns)
    
    return 1

remove_namespaces()