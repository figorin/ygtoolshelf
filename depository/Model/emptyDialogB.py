# -*- coding: utf-8 -*-

import sys
import logging
import json

from Qt import QtWidgets as qw,QtCore,QtGui,IsPySide,IsPySide2
# 工具架启动的时候会自动载入Qt模块来适配各种Qt版本
# 如需移植Qt模块，请查看Qt.__file__

if IsPySide:
    from shiboken import wrapInstance
elif IsPySide2:
    from shiboken2 import wrapInstance

try:
    import maya.cmds as cmds
    import maya.mel as mel
    import maya.OpenMayaUI as omui
    import maya.OpenMaya as om
    import pymel.core.datatypes as dt
    def getMayaWindow():
        ptr = omui.MQtUtil.mainWindow()
        return wrapInstance(long(ptr), qw.QWidget)

    MayaParent = getMayaWindow()
    # maya相关模块加载
except:
    MayaParent = None


logging.basicConfig(level = logging.INFO,format = '%(levelname)s|%(asctime)s - %(name)s - %(funcName)s : %(message)s')
logger = logging.getLogger('newdialog')
logger.setLevel(logging.DEBUG)




class newdialog(qw.QMainWindow):

    @property
    def title(self):
        return self._title

    @title.setter
    def title(self,val):
        if isinstance(val,str):
            self._title = val
            self.setWindowTitle(val)

    @property
    def x(self):
        return self._x

    @x.setter
    def x(self,val):
        if isinstance(val,int):
            self._x = val
            self.resize(val,self.y)
    @property
    def y(self):
        return self._y

    @y.setter
    def y(self,val):
        if isinstance(val,int):
            self._y = val
            self.resize(self.x,val)


    def __init__(self,title=u'模型整理工具_我的三体',parent=None):
        #super(newdialog,self).__init__(parent)
        qw.QMainWindow.__init__(self)
        # 2.7和3.0的super写法与工具架不兼容，请使用2.2写法。


        self._title='' 
        self._x=0 
        self._y=0
        self.m_DragPosition=self.pos()
        self.m_drag=False
        # 预设变量

        self.x = 400
        self.y = 300
        # 设置默认窗口大小
        self.move(0,0)
        # 打开窗口后，窗口默认的位置

        self.title = title
        self.initData()
        self.readSettings()
        # 读取窗口设定

        self.setWindowFlags(QtCore.Qt.SubWindow|QtCore.Qt.FramelessWindowHint | QtCore.Qt.WindowStaysOnTopHint)
        # 子窗口，无边框，永远在最上

        self.setMouseTracking(True)
        # 鼠标追踪，适用于无标题栏拖拽

        # 预留窗体边框
        self.Margins = 10
        

        # 创建左键鼠标标志
        self.mL_click = False
        # 创建右键鼠标标志
        self.mR_clickLeft = False
        self.mR_clickUp = False
        self.mR_clickRight = False
        self.mR_clickDown = False

        # 窗口左
        self.winLeftOutX = self.pos().x()
        self.winLeftInX = self.winLeftOutX + self.Margins
        # 窗口右
        self.winRightOutX = self.winLeftOutX + self.size().width()
        self.winRightInX = self.winRightOutX - self.Margins
        # 窗口上
        self.winUpOutY = self.pos().y()
        self.winUpInY = self.winUpOutY + self.Margins
        # 窗口下
        self.winDownOutY = self.winUpOutY + self.size().height()
        self.winDownInY = self.winDownOutY - self.Margins



        self.createUI()






    def createUI(self):
        mian=qw.QWidget(self)
        self.setCentralWidget(mian)
        self.mv = qw.QVBoxLayout()
        mian.setLayout(self.mv)
        # 主框架，设置主要在self.mv上
                                               
        titleLine = self.createSubTitleLine()
        self.mv.addLayout( titleLine )
        # 伪标题栏

        #------main btn-----------------------
        
        

    # =====================================================================================
    # 以下内容是模板内容，请参考注释酌情修改



    def createSubTitleLine(self,height=25):
        # 模拟用的标题栏
        rtLine = qw.QHBoxLayout()
        Xbtn=qw.QPushButton('x')
        Xbtn.setFixedHeight(height)
        Xbtn.setFixedWidth(height)
        rtLine.addWidget(Xbtn)
        rtLine.addStretch()
        #-----TitleLabel-----------------------
        TitleLab=qw.QLabel(u'空白窗口栏')
        TitleLab.setFixedHeight(height)
        rtLine.addWidget(TitleLab)
        rtLine.addStretch()
        Xbtn.clicked.connect(self.Cl_Ui)
        return rtLine

    def judgeReigon(self, pos):
        # 鼠标在窗口左侧
        if self.winLeftOutX < pos.x() < self.winLeftInX and not self.winDownInY < pos.y() < self.winDownOutY and not self.winUpOutY < pos.y() < self.winUpInY:
            self.mR_clickLeft = True
            self.setCursor(QtCore.Qt.SplitHCursor)
        # 鼠标在窗口右侧
        elif self.winRightInX < pos.x() < self.winRightOutX and not self.winDownInY < pos.y() < self.winDownOutY and not self.winUpOutY < pos.y() < self.winUpInY:
            self.mR_clickRight = True
            self.setCursor(QtCore.Qt.SplitHCursor)
        # 鼠标在窗口上方
        elif self.winUpOutY < pos.y() < self.winUpInY and not self.winRightInX < pos.x() < self.winRightOutX and not self.winLeftOutX < pos.x() < self.winLeftInX:
            self.mR_clickUp = True
            self.setCursor(QtCore.Qt.SplitVCursor)
        # 鼠标在窗口下方
        elif self.winDownInY < pos.y() < self.winDownOutY:
            self.mR_clickDown = True
            self.setCursor(QtCore.Qt.SplitVCursor)


    def initData(self):
        self.dataSettings = QtCore.QSettings("customDialog - subwindows", "newdialog")
        # 创建和读取qsetting的设置
        # 前一项是组，后一个是项，为了避免串数据，请单独设置组和项的名字。

    def readSettings(self):
        self.dataSettings.beginGroup('customDialog - newdialog - main')
        windowGeometry = self.dataSettings.value('window_geometry')
        windowState = self.dataSettings.value('window_state')
        self.restoreGeometry(windowGeometry)
        self.restoreState(windowState)
        self.dataSettings.endGroup()

    def writeSettings(self):
        self.dataSettings.beginGroup('customDialog - newdialog - main')
        self.dataSettings.setValue('window_geometry', self.saveGeometry())
        self.dataSettings.setValue('window_state', self.saveState())
        self.dataSettings.endGroup()

        
    def mousePressEvent(self,event):

        if event.button() == QtCore.Qt.LeftButton:
            self.m_drag=True
            self.mL_click = True
            self.m_DragPosition=event.globalPos()-self.pos()
            event.accept()
        elif event.button() == QtCore.Qt.RightButton:
            self.judgeReigon(event.globalPos())
            event.accept()
            
    def mouseMoveEvent(self,QMouseEvent):

        if QMouseEvent.buttons() == QtCore.Qt.LeftButton:
            self.move(QMouseEvent.globalPos()-self.m_DragPosition)
            QMouseEvent.accept()
        elif QMouseEvent.buttons() == QtCore.Qt.RightButton:
            self.atLeft = self.winLeftOutX < QMouseEvent.globalPos().x() < self.winLeftInX
            self.atRight = self.winRightInX < QMouseEvent.globalPos().x() < self.winRightOutX
            self.atUp = self.winUpOutY < QMouseEvent.globalPos().y() < self.winUpInY
            self.atDown = self.winDownInY < QMouseEvent.globalPos().y() < self.winDownOutY
            self.atCenter = self.winLeftInX < QMouseEvent.globalPos().x() < self.winRightInX and self.winUpInY < QMouseEvent.globalPos().y() < self.winDownInY

            # 只用一个变量来表示任意一侧边界被点击的状态 注意这里用的不是atLeft之类的变量 而是clickLeft之类的用来表示鼠标点击状态的变量
            # 因为atLeft之类的变量只是用来表示鼠标是否移动到了某个区域的情况的
            # 当鼠标在移动的过程中 一旦鼠标光标超过了atLeft的范围 这个变量的值就会变为假 一旦为假 在后面需要调整大小的时候 触发条件便会不成立
            mR_clickBorder = self.mR_clickDown or self.mR_clickLeft or self.mR_clickUp or self.mR_clickRight

            # 更改窗口位置
            if self.mL_click:
                moveVec = QMouseEvent.globalPos() - self.m_Position
                self.move(moveVec)
                # 更新边界位置
                # 窗口左
                self.winLeftOutX = self.pos().x()
                self.winLeftInX = self.winLeftOutX + self.Margins
                # 窗口右
                self.winRightOutX = self.winLeftOutX + self.size().width()
                self.winRightInX = self.winRightOutX - self.Margins
                # 窗口上
                self.winUpOutY = self.pos().y()
                self.winUpInY = self.winUpOutY + self.Margins
                # 窗口下
                self.winDownOutY = self.winUpOutY + self.size().height()
                self.winDownInY = self.winDownOutY - self.Margins

            # 更改鼠标图标
            # 在左方
            if self.atLeft:
                self.setCursor(QtCore.Qt.SplitHCursor)
            # 在右方
            if self.atRight:
                self.setCursor(QtCore.Qt.SplitHCursor)
            # 在上方
            if self.atUp:
                self.setCursor(QtCore.Qt.SplitVCursor)
            # 在下方
            if self.atDown:
                self.setCursor(QtCore.Qt.SplitVCursor)
            # 在中央并且没有单击右键 判定此刻没有单击右键很重要 否则鼠标光标在按住右键并移动到中心区域时会在两种图标间闪烁切换
            if self.atCenter and not mR_clickBorder:
                self.setCursor(QtCore.Qt.ArrowCursor)
            # 如果点击了窗口边界则调整大小
            if mR_clickBorder:
                self.resizeWin(QMouseEvent.globalPos())
            
    def mouseReleaseEvent(self,QMouseEvent):
        if QMouseEvent.button() == QtCore.Qt.LeftButton:
            self.mL_click = False
            self.m_drag=False
        if QMouseEvent.button() == QtCore.Qt.RightButton:
            self.mR_clickLeft = False
            self.mR_clickUp = False
            self.mR_clickRight = False
            self.mR_clickDown = False
            
            self.setCursor(QtCore.Qt.ArrowCursor)

    def resizeWin(self, mgPos):
        winX = self.geometry().x()
        winY = self.geometry().y()
        winW = self.geometry().width()
        winH = self.geometry().height()
        downside = self.pos().y() + winH
        rightside = self.pos().x() + winW

        # 调整上方边界
        if self.mR_clickUp and (downside - self.maximumSize().height()) < mgPos.y() < downside - self.minimumSize().height():
            winY = mgPos.y()
            winH = downside - winY
            self.winUpOutY = winY
            self.winUpInY = self.winUpOutY + self.Margins
            self.setGeometry(winX, winY, winW, winH)
        # 调整下方边界
        if self.mR_clickDown and (winY + self.minimumSize().height()) < mgPos.y() < (winY + self.maximumSize().height()):
            winH = mgPos.y() - self.pos().y()
            self.winDownOutY = winH + self.pos().y()
            self.winDownInY = self.winDownOutY - self.Margins
            self.setGeometry(winX, winY, winW, winH)
        # 调整左侧边界
        if self.mR_clickLeft and (rightside - self.maximumSize().width()) < mgPos.x() < rightside - self.minimumSize().width():
            winX = mgPos.x()
            winW = rightside - winX
            self.winLeftOutX = winX
            self.winLeftInX = self.winLeftOutX + self.Margins
            self.setGeometry(winX, winY, winW, winH)
        # 调整右侧边界
        if self.mR_clickRight and (winX + self.minimumSize().width()) < mgPos.x() < (winX + self.maximumSize().width()):
            winW = mgPos.x() - self.pos().x()
            self.winRightOutX = winW + self.pos().x()
            self.winRightInX = self.winRightOutX - self.Margins
            self.setGeometry(winX, winY, winW, winH)
    def closeEvent(self, event):
        try:
            self.writeSettings()
        except:
            pass
        # 写入临时信息，比如位置大小等
        self.deleteLater()  
        # 在窗口关闭的时候触发销毁自己的行为

    def Op_Ui(self):
        # 显示接口
        self.show()
        
    def Cl_Ui(self):
        # 关闭接口
        self.close()

    # 以上内容是模板内容，请参考注释酌情修改
    # =====================================================================================

global titleVar
titleVar = newdialog()
titleVar.Op_Ui()

    
    
    
    
    
    
    
    