# -*- coding: utf-8 -*-
'''
@Date: 2020-04-26 00:07:56
@LastEditors: figo
@LastEditTime: 2020-04-26 01:48:21
@FilePath: \gitLab\sstoolshelf\debug\depository\Model\ModelRestructDialog.py
'''
try:
    import maya.cmds as cmds
    import maya.mel as mel
    import maya.OpenMayaUI as omui
    import maya.OpenMaya as om
    import pymel.core.datatypes as dt
    def getMayaWindow():
        ptr = omui.MQtUtil.mainWindow()
        return wrapInstance(long(ptr), qw.QWidget)

    MayaParent = getMayaWindow()
    # maya相关模块加载
except:
    MayaParent = None

from Qt import QtWidgets as qw,QtCore,QtGui,IsPySide,IsPySide2
# 工具架启动的时候会自动载入Qt模块来适配各种Qt版本
# 如需移植Qt模块，请查看Qt.__file__

if IsPySide:
    from shiboken import wrapInstance
elif IsPySide2:
    from shiboken2 import wrapInstance

import sys
import logging
import json

logging.basicConfig(level = logging.INFO,format = '%(levelname)s|%(asctime)s - %(name)s - %(funcName)s : %(message)s')
logger = logging.getLogger('ModelRestructDialog')
logger.setLevel(logging.DEBUG)




class ModelRestructDialog(qw.QMainWindow):

    @property
    def title(self):
        return self._title

    @title.setter
    def title(self,val):
        if isinstance(val,str):
            self._title = val
            self.setWindowTitle(val)

    @property
    def x(self):
        return self._x

    @x.setter
    def x(self,val):
        if isinstance(val,int):
            self._x = val
            self.resize(val,self.y)
    @property
    def y(self):
        return self._y

    @y.setter
    def y(self,val):
        if isinstance(val,int):
            self._y = val
            self.resize(self.x,val)


    def __init__(self,title=u'模型整理工具_我的三体',parent=None):
        #super(ModelRestructDialog,self).__init__(parent)
        qw.QMainWindow.__init__(self)
        # 2.7和3.0的super写法与工具架不兼容，请使用2.2写法。


        self._title='' 
        self._x=0 
        self._y=0
        self.m_DragPosition=self.pos()
        self.m_drag=False
        # 预设变量

        self.x = 400
        self.y = 300
        # 设置默认窗口大小
        self.move(0,0)
        # 打开窗口后，窗口默认的位置

        self.title = title
        self.initData()
        self.readSettings()
        # 读取窗口设定

        self.setWindowFlags(QtCore.Qt.SubWindow|QtCore.Qt.FramelessWindowHint | QtCore.Qt.WindowStaysOnTopHint)
        # 子窗口，无边框，永远在最上

        self.setMouseTracking(True)
        # 鼠标追踪，适用于无标题栏拖拽


                
        self.ARRAY_BODY = ['body_subadds_mod_grp',
                            'body_hand_mod_grp',
                            'body_limb_mod_grp',
                            'body_main_mod_grp']


        self.ARRAY_HEAD = ['head_eyelash_mod_grp',
                            'head_eye_mod_grp',
                            'head_tongue_mod_grp',
                            'head_teeth_mod_grp',
                            'head_brow_mod_grp',
                            'head_hair_mod_grp',
                            'head_main_mod_grp',
                            'head_subadds_mod_grp']

        self.ARRAY_SUBPART = ['subparts_mod_grp']

        self.ARRAY_PROXY = ['proxy_grp']




        self.matchDict = {}



        self.createUI()




    def createSubTitleLine(self,height=25):
        rtLine = qw.QHBoxLayout()
        Xbtn=qw.QPushButton('x')
        Xbtn.setFixedHeight(height)
        Xbtn.setFixedWidth(height)
        rtLine.addWidget(Xbtn)
        rtLine.addStretch()
        
        
        #-----TitleLabel-----------------------
        TitleLab=qw.QLabel(u'我三S4模型整理工具')
        TitleLab.setFixedHeight(height)
        rtLine.addWidget(TitleLab)
        rtLine.addStretch()

        Xbtn.clicked.connect(self.Cl_Ui)
        return rtLine


    def createPickLine(self,paraname,labwid = 120,labht = 25):
        box = qw.QHBoxLayout()
        label = qw.QLabel(paraname)
        #label.setFixedWidth(labwid)
        box.addWidget(label)
        pushBtn = qw.QPushButton('<<')
        pushBtn.setFixedHeight(labht)
        pushBtn.setFixedWidth(50)
        pushBtn.setObjectName(paraname + '_pushButton')
        pushBtn.setProperty('groupname',paraname)
        box.addWidget(pushBtn)

        
        selBtn = qw.QPushButton('sel')
        selBtn.setFixedHeight(labht)
        selBtn.setFixedWidth(50)
        selBtn.setObjectName(paraname + '_sel')
        selBtn.setProperty('groupname',paraname)
        box.addWidget(selBtn)
        
        showBtn = qw.QPushButton('show')
        showBtn.setFixedHeight(labht)
        showBtn.setFixedWidth(50)
        showBtn.setObjectName(paraname + '_show')
        showBtn.setProperty('groupname',paraname)
        box.addWidget(showBtn)

        pushBtn.clicked.connect(self.pickFn)
        selBtn.clicked.connect(self.selFn)
        showBtn.clicked.connect(self.showFn)
        return {paraname:[box,selBtn,pushBtn,showBtn]}

    def createUI(self):
        mian=qw.QWidget(self)
        self.setCentralWidget(mian)
        self.mv = qw.QVBoxLayout()
        mian.setLayout(self.mv)
        # 主框架，设置主要在self.mv上
                                               
        titleLine = self.createSubTitleLine()
        self.mv.addLayout( titleLine )
        # 伪标题栏

        #------main btn-----------------------
        basicLine = qw.QHBoxLayout()

        nameLab = qw.QLabel('name')
        basicLine.addWidget(nameLab)
        self.nameEdit = qw.QLineEdit()
        basicLine.addWidget(self.nameEdit)

        comboList = [u'Characters - 角色',u'Props - 道具',u'Sets - 场景整合',u'Elements - 场景元素']

        self.cataComb = qw.QComboBox()
        self.cataComb.addItems(comboList)
        basicLine.addWidget(self.cataComb)

        self.varientEdit = qw.QLineEdit()
        self.varientEdit.setText('default')
        basicLine.addWidget(self.varientEdit)
        self.mv.addLayout(basicLine)


        self.BtnA1=qw.QPushButton(u'刷新/创建基本组')
        self.BtnA1.setEnabled(True)
        self.BtnA1.setFixedHeight(20)
        self.BtnA1.setFixedWidth(60)
        # button的一些设定

        self.mv.addWidget(self.BtnA1)
        # 将btn添加到layout中  

        self.tabwidget = qw.QTabWidget()
        self.mv.addWidget(self.tabwidget)

        chaDefaultWid = qw.QWidget()
        chalay = qw.QVBoxLayout()
        chaDefaultWid.setLayout(chalay)
        

        #ARRAY_BODY + ARRAY_HEAD + ARRAY_SUBPART + ARRAY_PROXY
        loopDict = {
            u'身体和衣服模型':self.ARRAY_BODY,
            u'头部和头发模型':self.ARRAY_HEAD,
            u'装备和武器模型':self.ARRAY_SUBPART,
            u'解算与代理模型':self.ARRAY_PROXY

        }
        
        for key,val in loopDict.items():
            keyLab = qw.QLabel(key)
            chalay.addWidget(keyLab)

            for mark in val:
                #mark = 'proxy'
                testList = self.createPickLine(mark)
                chalay.addLayout(testList[mark][0])
                self.matchDict.update(testList)
                
        self.tabwidget.addTab(chaDefaultWid,u'角色默认')


        userWid = qw.QWidget()
        self.userlay = qw.QVBoxLayout()
        userWid.setLayout(self.userlay)

        userlab = qw.QLabel()
        userlab.setText(u'输入希望新建的部位名字，然后点 + 号键')
        self.userlay.addWidget(userlab)

        addLine = qw.QHBoxLayout()
        self.userlay.addLayout(addLine)
        addBtn = qw.QPushButton()
        addBtn.setText('+')
        addLine.addWidget(addBtn)
        self.inputLine = qw.QLineEdit()
        self.inputLine.setText('subparts')
        addLine.addWidget(self.inputLine)
        suffixLab = qw.QLabel()
        suffixLab.setText('_mod_grp')
        addLine.addWidget(suffixLab)

        self.tabwidget.addTab(userWid,u'自定义')
        self.userlay.addStretch()

        self.BtnA1.clicked.connect(self.BtnFn)
        addBtn.clicked.connect(self.addFn)
        # 给button绑定一个功能，新式写法
        

    # =====================================================================================
    # 以下内容是模板内容，请参考注释酌情修改

    def addFn(self):
        mark = self.inputLine.text() + '_mod_grp'
        theItem = self.createPickLine(mark )
        self.userlay.insertLayout(0,theItem[mark][0])

    def initData(self):
        self.dataSettings = QtCore.QSettings("SStoolshelf - subwindows", "ModelRestructDialog")
        # 创建和读取qsetting的设置
        # 前一项是组，后一个是项，为了避免串数据，请单独设置组和项的名字。

    def readSettings(self):
        self.dataSettings.beginGroup('SStoolshelf - ModelRestructDialog - main')
        windowGeometry = self.dataSettings.value('window_geometry')
        windowState = self.dataSettings.value('window_state')
        self.restoreGeometry(windowGeometry)
        self.restoreState(windowState)
        self.dataSettings.endGroup()

    def writeSettings(self):
        self.dataSettings.beginGroup('SStoolshelf - ModelRestructDialog - main')
        self.dataSettings.setValue('window_geometry', self.saveGeometry())
        self.dataSettings.setValue('window_state', self.saveState())
        self.dataSettings.endGroup()

        
    def mousePressEvent(self,event):

        if event.button() == QtCore.Qt.LeftButton or event.button() == QtCore.Qt.RightButton:
            self.m_drag=True
            self.m_DragPosition=event.globalPos()-self.pos()
            event.accept()
            
    def mouseMoveEvent(self,QMouseEvent):

        if QMouseEvent.buttons() and QtCore.Qt.LeftButton:
            self.move(QMouseEvent.globalPos()-self.m_DragPosition)
            QMouseEvent.accept()
            
    def mouseReleaseEvent(self,QMouseEvent):
        self.m_drag=False

    def closeEvent(self, event):
        self.writeSettings()
        # 写入临时信息，比如位置大小等
        self.deleteLater()  
        # 在窗口关闭的时候触发销毁自己的行为

    # 以上内容是模板内容，请参考注释酌情修改
    # =====================================================================================

    def createGruop(self,name = '',empty=0):
        check = cmds.objExists(name)
        if check:
            try:
                cmds.parent(name,world=True)
            except:
                pass
        else:
            cmds.group(empty=1,n=name)
        
        return name

    def createbasic(self,itemname,itemcata,itemvari):
        # itemname = 'chengxin'
        # itemcata = 'charactor' # charactor,prop,set,environment
        # itemvari = 'default'
        # itemvers = 1
        
        top = '%s_%s_mod_all_grp' % (itemcata[0],itemname)
        self.createGruop(empty=1,name=top)
        
        array1 = self.ARRAY_PROXY  + ['geometry_grp']
        for i in array1:
            self.createGruop(empty=1,name=i)
            cmds.parent(i,top)



        indentifyArray = {"name":itemname,"cata":itemcata,"varient":itemvari,"version":0,'dep':'MODEL'}
        if not  cmds.objExists('geometry_grp.item_indentify'):
            cmds.addAttr('geometry_grp',ln = 'item_indentify',dt='string')
        else:
            cmds.setAttr('geometry_grp.item_indentify',l=0,e=1)
        cmds.setAttr('geometry_grp.item_indentify',json.dumps(indentifyArray),type='string',l=1,e=1)

            
        dupGrp = 'duplicate_layer_grp'
        self.createGruop(empty=1,name=dupGrp)
        cmds.parent(dupGrp,array1[-1])
        
        # array_geo = ['body_mod_grp','head_mod_grp'] + self.ARRAY_SUBPART
        # for i in array_geo:
        #     self.createGruop(empty=1,name=i)
        #     cmds.parent(i,dupGrp)
            
        # array_body = list(self.ARRAY_BODY)
        # for i in array_body:
        #     self.createGruop(empty=1,name=i)
        #     cmds.parent(i,array_geo[0])
            
        
        # array_head = list(self.ARRAY_HEAD)
        # for i in array_head:
        #     self.createGruop(empty=1,name=i)
        #     cmds.parent(i,array_geo[1])
            

    def pickFn(self):
        sel = cmds.ls(sl=1)
        if sel:
            sender = self.sender()
            keyStr = sender.property('groupname')
            logger.debug(sender.property('groupname'))

            if keyStr not in ['proxy_grp']:
                theGrp = self.createGruop(name=keyStr)
                cmds.parent(theGrp,'duplicate_layer_grp')
            else:
                raise RuntimeError (u'proxy_grp not exists.please recreate struct')

            
            removeLast = '_'.join(keyStr.split('_')[:-1])
            getExists = cmds.ls(removeLast + '_*',type='transform')
            if keyStr in getExists:
                getExists.remove(keyStr)
            maxNum = 0
            if len(getExists)>0:
                getExists.sort()
                maxNum = int(getExists[-1].split('_')[-1])

            for count,i in enumerate(sel):
                if i in getExists:
                    continue
                newname = removeLast + '_' + str(count+maxNum).zfill(2)
                cmds.rename(i,newname)

            getExists = cmds.ls(removeLast + '_*',type='transform')
            if keyStr in getExists:getExists.remove(keyStr)
            for i in getExists:
                cmds.setAttr('%s.v'%i,0)
                if cmds.objExists(keyStr):
                    try:
                        cmds.parent(i,keyStr)
                    except:
                        pass
                    

            sender.setText('%s <<' % str(len(getExists)))

    def selFn(self):
        sender = self.sender()
        keyStr = sender.property('groupname')
        
        logger.debug(sender.property('groupname'))
        removeLast = '_'.join(keyStr.split('_')[:-1])
        getExists = cmds.ls(removeLast + '_*',type='transform')
        if keyStr in getExists:
            getExists.remove(keyStr)
            
        if getExists:
            cmds.select(getExists)

    def showFn(self):
        sender = self.sender()
        keyStr = sender.property('groupname')
        
        logger.debug(sender.property('groupname'))
        removeLast = '_'.join(keyStr.split('_')[:-1])
        getExists = cmds.ls(removeLast + '_*',type='transform')
        if getExists:
            if sender.text() == 'show':
                for i in getExists:
                    try:
                        cmds.setAttr(i + '.v',1)
                    except:
                        print 'item %s.visable attr is lock or cannot modify' % i
                sender.setText('hide')
            else:
                for i in getExists:
                    try:
                        cmds.setAttr(i + '.v',0)
                    except:
                        print 'item %s.visable attr is lock or cannot modify' % i
                sender.setText('show')


    def BtnFn(self):
        logger.info('press button')
        itemname = self.nameEdit.text()
        itemcata = self.cataComb.currentText().split(' ')[0]
        itemvari = self.varientEdit.text()
        self.createbasic(itemname,itemcata,itemvari)

    def Op_Ui(self):
        # 显示接口
        self.show()
        
    def Cl_Ui(self):
        # 关闭接口
        self.close()
global titleVar
titleVar = ModelRestructDialog()
titleVar.Op_Ui()

# itemname = 'chengxin'
# itemcata = 'character'
# itemvari = 'default'
# itemvers = 1


                            
#createbasic()
    
    
    
    
    
    
    
    