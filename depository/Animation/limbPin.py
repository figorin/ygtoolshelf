import maya.cmds as cmds

targetList = ['HipPart1_R','HipPart1_L','ShoulderPart1_L','ShoulderPart1_R']

for i in targetList:
    node = cmds. spaceLocator(n=i+'_loc')
    nodeName = node[0]
    nodegrp = cmds.group(node,n=nodeName + '_grp')
    nodesdk = cmds.group(nodegrp,n=nodeName + '_sdk')
    nodezero = cmds.group(nodesdk,n=nodeName + '_zero')
    
    cmds.parentConstraint(i,nodezero,mo=0,w=1)
    cmds.setAttr("%sShape.localScaleX"%nodeName,35)
    cmds.setAttr("%sShape.localScaleY"%nodeName,35)
    cmds.setAttr("%sShape.localScaleZ"%nodeName,35)