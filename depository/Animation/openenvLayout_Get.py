'''
@Date: 2020-04-25 22:29:07
LastEditors: figo-ds
LastEditTime: 2020-12-24 16:17:10
FilePath: \undefinedc:\gitlab\sstoolshelf\debug\FgToolshelf\depository\Animation\openenvLayout_Get.py
'''
import sys
import os
try:
    toolPath = os.environ['FGPIPEROOT']
except KeyError:
    # toolPath = r'S:\PipeProgram\figoPipe\pipelineMainProgram'
    toolPath = r'C:\gitLab\brownser01'
finally:
    if toolPath not in sys.path:
        sys.path.append(toolPath)

import figoBrownser.platforms.maya.animate.Browser_EnvLayout_Get as seq_get
reload(seq_get)

global FGbrownserWindow
FGbrownserWindow = None
FGbrownserWindow = seq_get.main()