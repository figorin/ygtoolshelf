'''
@Date: 2020-04-25 22:29:07
LastEditors: figo - uz
LastEditTime: 2020-12-24 14:33:14
FilePath: \undefinedc:\gitlab\sstoolshelf\debug\FgToolshelf\depository\Animation\openItemToolDialog_EnvLayout.py
'''
import sys
import os
try:
    toolPath = os.environ['FGPIPEROOT']
except KeyError:
    # toolPath = r'S:\PipeProgram\figoPipe\pipelineMainProgram'
    toolPath = r'C:\gitLab\brownser01'
finally:
    if toolPath not in sys.path:
        sys.path.append(toolPath)

import figoBrownser.platforms.maya.animate.Browser_EnvLayout_ItemTool as envlayout_tool
reload(envlayout_tool)

global FGbrownserWindow
FGbrownserWindow = None
FGbrownserWindow = envlayout_tool.main()