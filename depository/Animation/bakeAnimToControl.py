

# -*- coding: utf-8 -*-

import sys
import logging
import json
import maya.api.OpenMaya as om
import maya.api.OpenMayaAnim as oma
import json
import maya.cmds as cmds

from Qt import QtWidgets as qw,QtCore,QtGui,IsPySide,IsPySide2
# 工具架启动的时候会自动载入Qt模块来适配各种Qt版本
# 如需移植Qt模块，请查看Qt.__file__

if IsPySide:
    from shiboken import wrapInstance
elif IsPySide2:
    from shiboken2 import wrapInstance

try:
    import maya.cmds as cmds
    import maya.mel as mel
    # import maya.OpenMayaUI as omui
    # import maya.OpenMaya as om
    import pymel.core.datatypes as dt
    def getMayaWindow():
        ptr = omui.MQtUtil.mainWindow()
        return wrapInstance(long(ptr), qw.QWidget)

    MayaParent = getMayaWindow()
    # maya相关模块加载
except:
    MayaParent = None


logging.basicConfig(level = logging.INFO,format = '%(levelname)s|%(asctime)s - %(name)s - %(funcName)s : %(message)s')
logger = logging.getLogger('keyToDrivenTool')
logger.setLevel(logging.DEBUG)




class keyToDrivenTool(qw.QMainWindow):

    @property
    def title(self):
        return self._title

    @title.setter
    def title(self,val):
        if isinstance(val,str):
            self._title = val
            self.setWindowTitle(val)

    @property
    def x(self):
        return self._x

    @x.setter
    def x(self,val):
        if isinstance(val,int):
            self._x = val
            self.resize(val,self.y)
    @property
    def y(self):
        return self._y

    @y.setter
    def y(self,val):
        if isinstance(val,int):
            self._y = val
            self.resize(self.x,val)


    def __init__(self,title=u'动画帧转驱动关键帧',parent=None):
        #super(keyToDrivenTool,self).__init__(parent)
        qw.QMainWindow.__init__(self)
        # 2.7和3.0的super写法与工具架不兼容，请使用2.2写法。


        self._title='' 
        self._x=0 
        self._y=0
        self.m_DragPosition=self.pos()
        self.m_drag=False
        # 预设变量

        self.x = 400
        self.y = 300
        # 设置默认窗口大小
        self.move(0,0)
        # 打开窗口后，窗口默认的位置

        self.title = title
        self.initData()
        self.readSettings()
        # 读取窗口设定

        self.setWindowFlags(QtCore.Qt.SubWindow|QtCore.Qt.FramelessWindowHint | QtCore.Qt.WindowStaysOnTopHint)
        # 子窗口，无边框，永远在最上

        self.setMouseTracking(True)
        # 鼠标追踪，适用于无标题栏拖拽

        # 预留窗体边框
        self.Margins = 10
        

        # 创建左键鼠标标志
        self.mL_click = False
        # 创建右键鼠标标志
        self.mR_clickLeft = False
        self.mR_clickUp = False
        self.mR_clickRight = False
        self.mR_clickDown = False

        # 窗口左
        self.winLeftOutX = self.pos().x()
        self.winLeftInX = self.winLeftOutX + self.Margins
        # 窗口右
        self.winRightOutX = self.winLeftOutX + self.size().width()
        self.winRightInX = self.winRightOutX - self.Margins
        # 窗口上
        self.winUpOutY = self.pos().y()
        self.winUpInY = self.winUpOutY + self.Margins
        # 窗口下
        self.winDownOutY = self.winUpOutY + self.size().height()
        self.winDownInY = self.winDownOutY - self.Margins


        self.curveDict = {}
        self.createUI()






    def createUI(self):
        mian=qw.QWidget(self)
        self.setCentralWidget(mian)
        self.mv = qw.QVBoxLayout()
        mian.setLayout(self.mv)
        # 主框架，设置主要在self.mv上

        titleLine = self.createSubTitleLine()
        self.mv.addLayout( titleLine )
        # 伪标题栏

        #------main btn-----------------------
        self.line1Lab = qw.QLabel(u'动画源数据时间范围')
        self.line1inputA = qw.QLineEdit()
        self.line1LabB = qw.QLabel(' to ')
        self.line1inputB = qw.QLineEdit()
        self.line1HBox = qw.QHBoxLayout()
        self.line1HBox.addWidget(self.line1Lab)
        self.line1HBox.addWidget(self.line1inputA)
        self.line1HBox.addWidget(self.line1LabB)
        self.line1HBox.addWidget(self.line1inputB)
        
        self.line2Lab = qw.QLabel(u'控制器时间范围')
        self.line2inputA = qw.QLineEdit()
        self.line2LabB = qw.QLabel(' to ')
        self.line2inputB = qw.QLineEdit()
        self.line2HBox = qw.QHBoxLayout()
        self.line2HBox.addWidget(self.line2Lab)
        self.line2HBox.addWidget(self.line2inputA)
        self.line2HBox.addWidget(self.line2LabB)
        self.line2HBox.addWidget(self.line2inputB)
        
        self.doitBtn = qw.QPushButton('do it')




        self.AreaBVBox = qw.QVBoxLayout()
        self.AreaBVBox.addLayout(self.line1HBox)
        self.AreaBVBox.addLayout(self.line2HBox)
        self.AreaAHBox = qw.QHBoxLayout()
        self.AreaAHBox.addLayout(self.AreaBVBox)
        self.AreaAHBox.addWidget(self.doitBtn)

        self.mv.addLayout(self.AreaAHBox)


        self.AreaEHBox = qw.QHBoxLayout()
        self.LeftLab = qw.QLabel(u'拾取控制器并列出属性')
        self.RightLab = qw.QLabel(u'拾取动画物体')
        self.AreaEHBox.addWidget(self.LeftLab)
        self.AreaEHBox.addStretch()
        self.AreaEHBox.addWidget(self.RightLab)

        self.mv.addLayout(self.AreaEHBox)

        self.AreaCHBox = qw.QHBoxLayout()
        self.LeftList = qw.QListWidget()
        self.RightList = qw.QListWidget()
        self.AreaCHBox.addWidget(self.LeftList)
        self.AreaCHBox.addWidget(self.RightList)

        self.mv.addLayout(self.AreaCHBox)

        self.AreaDHBox = qw.QHBoxLayout()
        self.LeftBtn = qw.QPushButton()
        self.RightBtn = qw.QPushButton()
        self.AreaDHBox.addWidget(self.LeftBtn)
        self.AreaDHBox.addWidget(self.RightBtn)

        self.mv.addLayout(self.AreaDHBox)

        
        self.LeftBtn.clicked.connect(self.getUserDefinedAttr)
        self.RightBtn.clicked.connect(self.getDrivens)
        self.doitBtn.clicked.connect(self.doitFn)
    # =====================================================================================
    # 以下内容是模板内容，请参考注释酌情修改



    def createSubTitleLine(self,height=25):
        # 模拟用的标题栏
        rtLine = qw.QHBoxLayout()
        Xbtn=qw.QPushButton('x')
        Xbtn.setFixedHeight(height)
        Xbtn.setFixedWidth(height)
        rtLine.addWidget(Xbtn)
        rtLine.addStretch()
        #-----TitleLabel-----------------------
        TitleLab=qw.QLabel(u'动画帧转驱动关键帧')
        TitleLab.setFixedHeight(height)
        rtLine.addWidget(TitleLab)
        rtLine.addStretch()
        Xbtn.clicked.connect(self.Cl_Ui)
        return rtLine

    def judgeReigon(self, pos):
        # 鼠标在窗口左侧
        if self.winLeftOutX < pos.x() < self.winLeftInX and not self.winDownInY < pos.y() < self.winDownOutY and not self.winUpOutY < pos.y() < self.winUpInY:
            self.mR_clickLeft = True
            self.setCursor(QtCore.Qt.SplitHCursor)
        # 鼠标在窗口右侧
        elif self.winRightInX < pos.x() < self.winRightOutX and not self.winDownInY < pos.y() < self.winDownOutY and not self.winUpOutY < pos.y() < self.winUpInY:
            self.mR_clickRight = True
            self.setCursor(QtCore.Qt.SplitHCursor)
        # 鼠标在窗口上方
        elif self.winUpOutY < pos.y() < self.winUpInY and not self.winRightInX < pos.x() < self.winRightOutX and not self.winLeftOutX < pos.x() < self.winLeftInX:
            self.mR_clickUp = True
            self.setCursor(QtCore.Qt.SplitVCursor)
        # 鼠标在窗口下方
        elif self.winDownInY < pos.y() < self.winDownOutY:
            self.mR_clickDown = True
            self.setCursor(QtCore.Qt.SplitVCursor)


    def initData(self):
        self.dataSettings = QtCore.QSettings("customDialog - subwindows", "keyToDrivenTool")
        # 创建和读取qsetting的设置
        # 前一项是组，后一个是项，为了避免串数据，请单独设置组和项的名字。

    def readSettings(self):
        self.dataSettings.beginGroup('customDialog - keyToDrivenTool - main')
        windowGeometry = self.dataSettings.value('window_geometry')
        windowState = self.dataSettings.value('window_state')
        self.restoreGeometry(windowGeometry)
        self.restoreState(windowState)
        self.dataSettings.endGroup()

    def writeSettings(self):
        self.dataSettings.beginGroup('customDialog - keyToDrivenTool - main')
        self.dataSettings.setValue('window_geometry', self.saveGeometry())
        self.dataSettings.setValue('window_state', self.saveState())
        self.dataSettings.endGroup()

        
    def mousePressEvent(self,event):

        if event.button() == QtCore.Qt.LeftButton:
            self.m_drag=True
            self.mL_click = True
            self.m_DragPosition=event.globalPos()-self.pos()
            event.accept()
        elif event.button() == QtCore.Qt.RightButton:
            self.judgeReigon(event.globalPos())
            event.accept()
            
    def mouseMoveEvent(self,QMouseEvent):

        if QMouseEvent.buttons() == QtCore.Qt.LeftButton:
            self.move(QMouseEvent.globalPos()-self.m_DragPosition)
            QMouseEvent.accept()
        elif QMouseEvent.buttons() == QtCore.Qt.RightButton:
            self.atLeft = self.winLeftOutX < QMouseEvent.globalPos().x() < self.winLeftInX
            self.atRight = self.winRightInX < QMouseEvent.globalPos().x() < self.winRightOutX
            self.atUp = self.winUpOutY < QMouseEvent.globalPos().y() < self.winUpInY
            self.atDown = self.winDownInY < QMouseEvent.globalPos().y() < self.winDownOutY
            self.atCenter = self.winLeftInX < QMouseEvent.globalPos().x() < self.winRightInX and self.winUpInY < QMouseEvent.globalPos().y() < self.winDownInY

            # 只用一个变量来表示任意一侧边界被点击的状态 注意这里用的不是atLeft之类的变量 而是clickLeft之类的用来表示鼠标点击状态的变量
            # 因为atLeft之类的变量只是用来表示鼠标是否移动到了某个区域的情况的
            # 当鼠标在移动的过程中 一旦鼠标光标超过了atLeft的范围 这个变量的值就会变为假 一旦为假 在后面需要调整大小的时候 触发条件便会不成立
            mR_clickBorder = self.mR_clickDown or self.mR_clickLeft or self.mR_clickUp or self.mR_clickRight

            # 更改窗口位置
            if self.mL_click:
                moveVec = QMouseEvent.globalPos() - self.m_Position
                self.move(moveVec)
                # 更新边界位置
                # 窗口左
                self.winLeftOutX = self.pos().x()
                self.winLeftInX = self.winLeftOutX + self.Margins
                # 窗口右
                self.winRightOutX = self.winLeftOutX + self.size().width()
                self.winRightInX = self.winRightOutX - self.Margins
                # 窗口上
                self.winUpOutY = self.pos().y()
                self.winUpInY = self.winUpOutY + self.Margins
                # 窗口下
                self.winDownOutY = self.winUpOutY + self.size().height()
                self.winDownInY = self.winDownOutY - self.Margins

            # 更改鼠标图标
            # 在左方
            if self.atLeft:
                self.setCursor(QtCore.Qt.SplitHCursor)
            # 在右方
            if self.atRight:
                self.setCursor(QtCore.Qt.SplitHCursor)
            # 在上方
            if self.atUp:
                self.setCursor(QtCore.Qt.SplitVCursor)
            # 在下方
            if self.atDown:
                self.setCursor(QtCore.Qt.SplitVCursor)
            # 在中央并且没有单击右键 判定此刻没有单击右键很重要 否则鼠标光标在按住右键并移动到中心区域时会在两种图标间闪烁切换
            if self.atCenter and not mR_clickBorder:
                self.setCursor(QtCore.Qt.ArrowCursor)
            # 如果点击了窗口边界则调整大小
            if mR_clickBorder:
                self.resizeWin(QMouseEvent.globalPos())
            
    def mouseReleaseEvent(self,QMouseEvent):
        if QMouseEvent.button() == QtCore.Qt.LeftButton:
            self.mL_click = False
            self.m_drag=False
        if QMouseEvent.button() == QtCore.Qt.RightButton:
            self.mR_clickLeft = False
            self.mR_clickUp = False
            self.mR_clickRight = False
            self.mR_clickDown = False
            
            self.setCursor(QtCore.Qt.ArrowCursor)

    def resizeWin(self, mgPos):
        winX = self.geometry().x()
        winY = self.geometry().y()
        winW = self.geometry().width()
        winH = self.geometry().height()
        downside = self.pos().y() + winH
        rightside = self.pos().x() + winW

        # 调整上方边界
        if self.mR_clickUp and (downside - self.maximumSize().height()) < mgPos.y() < downside - self.minimumSize().height():
            winY = mgPos.y()
            winH = downside - winY
            self.winUpOutY = winY
            self.winUpInY = self.winUpOutY + self.Margins
            self.setGeometry(winX, winY, winW, winH)
        # 调整下方边界
        if self.mR_clickDown and (winY + self.minimumSize().height()) < mgPos.y() < (winY + self.maximumSize().height()):
            winH = mgPos.y() - self.pos().y()
            self.winDownOutY = winH + self.pos().y()
            self.winDownInY = self.winDownOutY - self.Margins
            self.setGeometry(winX, winY, winW, winH)
        # 调整左侧边界
        if self.mR_clickLeft and (rightside - self.maximumSize().width()) < mgPos.x() < rightside - self.minimumSize().width():
            winX = mgPos.x()
            winW = rightside - winX
            self.winLeftOutX = winX
            self.winLeftInX = self.winLeftOutX + self.Margins
            self.setGeometry(winX, winY, winW, winH)
        # 调整右侧边界
        if self.mR_clickRight and (winX + self.minimumSize().width()) < mgPos.x() < (winX + self.maximumSize().width()):
            winW = mgPos.x() - self.pos().x()
            self.winRightOutX = winW + self.pos().x()
            self.winRightInX = self.winRightOutX - self.Margins
            self.setGeometry(winX, winY, winW, winH)
    def closeEvent(self, event):
        try:
            self.writeSettings()
        except:
            pass
        # 写入临时信息，比如位置大小等
        self.deleteLater()  
        # 在窗口关闭的时候触发销毁自己的行为

    def Op_Ui(self):
        # 显示接口
        self.show()
        
    def Cl_Ui(self):
        # 关闭接口
        self.close()

    # 以上内容是模板内容，请参考注释酌情修改
    # =====================================================================================

    def doitFn(self):
        tgs = self.RightList.count()

        tg = self.LeftBtn.text()
        att= self.LeftList.currentItem().text()

        target = '%s.%s' % (tg,att)
        targetTemp = '%s.%sTemp' % (tg,att)
        if not cmds.objExists(targetTemp):
            cmds.addAttr(tg,ln='%sTemp'%att,at = 'double',dv=0)

        driverList = [float(self.line2inputA.text()),float(self.line2inputB.text())]
        drivenList = [float(self.line1inputA.text()),float(self.line1inputB.text())]

        cmds.setAttr(target,driverList[0])
        cmds.setAttr(targetTemp,drivenList[0])

        cmds.setDrivenKeyframe(targetTemp,currentDriver = target )
        cmds.setAttr(target,driverList[1])
        cmds.setAttr(targetTemp,drivenList[1])

        cmds.setDrivenKeyframe(targetTemp,currentDriver = target )
        cmds.setAttr(target,driverList[0])

        dnCount = self.RightList.count()
        for i in range(dnCount):
            relatis = cmds.listConnections(self.RightList.item(i).text(),type='animCurve',scn=1)
            if relatis:
                for rlt in relatis:
                    cmds.connectAttr(targetTemp,'%s.input'% rlt,f=1)



    def getDrivens(self):
        sel = cmds.ls(sl=1)
        if sel:
            
            self.RightList.clear()
            self.RightList.addItems(sel)

            self.curveDict = {}
            for i in sel:
                print '>>>',i
                self.curveDict[i] = self.getAnimationCurveInfo(i)

            maxium = 0
            minium = 50000
            for key,val in self.curveDict.items():
                for ikey,ival in val.items(): 
                    # for kkey,kval in ival.items():
                    print ival['timeList'] 
                    if ival['timeList'] [0]<minium:
                        minium = ival['timeList'] [0]
                    if ival['timeList'] [-1]> maxium:
                        maxium = ival['timeList'] [-1]

            self.line1inputA.setText(str(minium))
            self.line1inputB.setText(str(maxium))

            self.line2inputA.setText(str(0))
            self.line2inputB.setText(str(1))

        else:
            pass

    def getUserDefinedAttr(self):
        sel = cmds.ls(sl=1)
        if sel:
            attrs =  cmds.listAttr(sel[0],v=1,k=1)
            self.LeftList.clear()
            self.LeftList.addItems(attrs)
            self.LeftBtn.setText(sel[0])
        else:
            return False

    def getAnimationCurveInfo(self,currentObject):
        # input to Mobject
        # currentObject = 'BC4_crv'
        objSelection = om.MSelectionList()
        objSelection.add(currentObject)
        # objMObjectTemp = om.MFn.MObject()
        objMObject = objSelection.getDependNode(0)

        # find atter
        mfnDependencyNode = om.MFnDependencyNode(objMObject)
        attributeCount = mfnDependencyNode.attributeCount()

        nodeAnimationCurveInfo = {}

        for attriIndex in range(attributeCount):
            attriMObject = mfnDependencyNode.attribute(attriIndex)
            mfnAttribute = om.MFnAttribute(attriMObject)
            attriName = mfnAttribute.name
            currentPlug = mfnDependencyNode.findPlug(attriName,1)
            connectedList =  currentPlug.connectedTo(1,0)
            if connectedList:
                print attriIndex,attriName
                print connectedList
                conNodeMObject = connectedList[0].node()
                if conNodeMObject.hasFn(om.MFn.kAnimCurve):
                    print connectedList[0],'\t anim curve is connected'
                else:
                    print connectedList[0],'\t some oter connection'

                mfnAnimCurve = oma.MFnAnimCurve(conNodeMObject)

                animCurveType = mfnAnimCurve.animCurveType
                print animCurveType
                preInfinity = mfnAnimCurve.preInfinityType
                postInfinity = mfnAnimCurve.postInfinityType
                weightedTangent = mfnAnimCurve.isWeighted

                numKeys = mfnAnimCurve.numKeys
                timeList = []
                valueList = []
                inTangentTypeList = []
                inTangentAngleList = []
                inTangentWeightList = []
                outTangentTypeList = []
                outTangentAngleList = []
                outTangentWeightList = []
                for keyIndex in range(numKeys):
                    #time
                    input = mfnAnimCurve.input(keyIndex)
                    mTime = om.MTime(input)
                    currenttime = mTime.value
                    print mTime,'\t',currenttime
                    timeList.append(currenttime)

                    #value
                    value = mfnAnimCurve.value(keyIndex)
                    valueList.append(value)

                    #inTangent
                    inTangentType = mfnAnimCurve.inTangentType(keyIndex)
                    inTangentTypeList.append(inTangentType)
                    inTangentAngleWeight = mfnAnimCurve.getTangentAngleWeight(keyIndex,1)
                    inTangentAngle = om.MAngle(inTangentAngleWeight[0])
                    inTangentAngleValue = inTangentAngle.value

                    inTangentAngleList.append(inTangentAngleValue)
                    inTangentWeightList.append(inTangentAngleWeight[1])

                    #inTangent
                    outTangentType = mfnAnimCurve.outTangentType(keyIndex)
                    outTangentTypeList.append(outTangentType)
                    outTangentAngleWeight = mfnAnimCurve.getTangentAngleWeight(keyIndex,0)
                    outTangentAngle = om.MAngle(outTangentAngleWeight[0])
                    outTangentAngleValue = outTangentAngle.value

                    outTangentAngleList.append(outTangentAngleValue)
                    outTangentWeightList.append(outTangentAngleWeight[1])

                attributeDataDict = {
                'animCurveType':animCurveType,
                'preInfinity':preInfinity,
                'postInfinity':postInfinity,
                'weightedTangent':weightedTangent,
                'timeList':timeList,
                'valueList':valueList,
                'inTangentTypeList':inTangentTypeList,
                'inTangentAngleList':inTangentAngleList,
                'inTangentWeightList':inTangentWeightList,
                'outTangentTypeList':outTangentTypeList,
                'outTangentAngleList':outTangentAngleList,
                'outTangentWeightList':outTangentWeightList,

                }
                nodeAnimationCurveInfo[attriName] = attributeDataDict
        return nodeAnimationCurveInfo

global keyToDriven
keyToDriven = keyToDrivenTool()
keyToDriven.Op_Ui()

    
    
    
    
    
    
    
    