'''
@Date: 2020-04-25 22:29:07
LastEditors: figo - uz
LastEditTime: 2020-12-24 14:32:56
FilePath: \undefinedc:\gitlab\sstoolshelf\debug\FgToolshelf\depository\Animation\openenvLayout_Pub.py
'''
import sys
import os
try:
    toolPath = os.environ['FGPIPEROOT']
except KeyError:
    # toolPath = r'S:\PipeProgram\figoPipe\pipelineMainProgram'
    toolPath = r'C:\gitLab\brownser01'
finally:
    if toolPath not in sys.path:
        sys.path.append(toolPath)

import figoBrownser.platforms.maya.animate.Browser_EnvLayout_Publish as seq_pub
reload(seq_pub)

global FGbrownserWindow
FGbrownserWindow = None
FGbrownserWindow = seq_pub.main()