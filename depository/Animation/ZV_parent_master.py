# -*- coding: utf-8 -*-
import os
import sys
import maya.cmds as cmds
scriptPath = os.environ['sstoolshelfpath'] + '/depository/Animation'

if scriptPath not in sys.path:
    sys.path.append(scriptPath)

import zvparentmaster.ui
zvparentmaster.ui.show()