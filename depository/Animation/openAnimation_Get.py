'''
@Date: 2020-04-25 22:29:07
LastEditors: figo-ds
LastEditTime: 2020-12-24 16:17:01
FilePath: \undefinedc:\gitlab\sstoolshelf\debug\FgToolshelf\depository\Animation\openAnimation_Get.py
'''
import sys
import os
try:
    toolPath = os.environ['FGPIPEROOT']
except KeyError:
    # toolPath = r'S:\PipeProgram\figoPipe\pipelineMainProgram'
    toolPath = r'C:\gitLab\brownser01'
finally:
    if toolPath not in sys.path:
        sys.path.append(toolPath)

import figoBrownser.platforms.maya.animate.Browser_Animate_Get as ani_get
reload(ani_get)

global FGbrownserWindow
FGbrownserWindow = None
FGbrownserWindow = ani_get.main()