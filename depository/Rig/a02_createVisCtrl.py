import maya.cmds as cmds
global cmds
def createVisCtrl():
    visCtrl=cmds.curve(d=1,p=[(-6,4,0),(-4,6,0),(-2,7,0),(2,7,0),(4,6,0),(6,4,0),(4,2,0),(2,1,0),(0,1,0),(2,2,0),(3,4,0),(2,6,0),(0,7,0),(-2,6,0),(-3,4,0),(-2,2,0),(0,1,0),(-2,1,0),(-4,2,0),(-6,4,0)],n='visable_ctrl')
    moveGrp = cmds.group(visCtrl, n=(visCtrl + '_move_grp'))
    visGrp=cmds.group(moveGrp,n=(visCtrl+'_grp'))
    if cmds.objExists('Head_M'):
        headPos=cmds.xform('Head_M',q=1,ws=1,t=1)
        cmds.xform(visGrp, ws=1, t=(headPos[0], (headPos[1] + 50), headPos[2]))
        cmds.parentConstraint('Head_M', visGrp, mo=1, weight=1)
        cmds.scaleConstraint('Head_M', visGrp, offset=[1, 1, 1], weight=1)
    else:
        headPos = [0, 0, 0]
        cmds.xform(visGrp, ws=1, t=(headPos[0], (headPos[1] + 50), headPos[2]))

    cmds.setAttr(visCtrl+".overrideEnabled",1)
    cmds.setAttr(visCtrl+".overrideColor",17)
    cmds.addAttr(visCtrl,ln="Model_Type",at="enum",en="normal:tengent:refrence",k=1)
    cmds.addAttr(visCtrl,ln="vis_SubCtrl",at="bool",k=1)
    cmds.setAttr(visCtrl+'.tx',lock=1,k=0,channelBox=0)
    cmds.setAttr(visCtrl+'.ty',lock=1,k=0,channelBox=0)
    cmds.setAttr(visCtrl+'.tz',lock=1,k=0,channelBox=0)
    cmds.setAttr(visCtrl+'.rx',lock=1,k=0,channelBox=0)
    cmds.setAttr(visCtrl+'.ry',lock=1,k=0,channelBox=0)
    cmds.setAttr(visCtrl+'.rz',lock=1,k=0,channelBox=0)
    cmds.setAttr(visCtrl+'.sx',lock=1,k=0,channelBox=0)
    cmds.setAttr(visCtrl+'.sy',lock=1,k=0,channelBox=0)
    cmds.setAttr(visCtrl+'.sz',lock=1,k=0,channelBox=0)
    cmds.setAttr(visCtrl+'.v',lock=1,k=0,channelBox=0)
    

cmds.undoInfo(openChunk=True)    
createVisCtrl()
cmds.undoInfo(closeChunk=True)