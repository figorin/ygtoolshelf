# -*- coding: utf-8 -*-
import os
import sys
import maya.cmds as cmds
scriptPath = os.environ['sstoolshelfpath'] + '/depository/Rig'

if scriptPath not in sys.path:
    sys.path.append(scriptPath)

import pyqtWidgets.curveShape as curveShape
reload(curveShape)

curveShapeUIRT=curveShape.CurveShape_RIG_TOOL()
curveShapeUIRT.createUI()
curveShapeUIRT.Op_Ui()