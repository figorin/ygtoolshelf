#!/usr/bin/env python
# coding=utf-8
'''
Author: figo project
Date: 2020-08-11 15:38:59
LastEditTime: 2020-08-17 14:17:10
LastEditors: figo
Description: 
FilePath: \gitLab\sstoolshelf\publish\FgToolshelf\depository\Rig\drop200811153859.py
copyright: copyright by figo 2020
'''
# -*- coding: utf-8 -*-
'''
@Date: 2020-04-26 00:07:56
@LastEditors: figo
@LastEditTime: 2020-04-26 01:48:21
@FilePath: \gitLab\sstoolshelf\debug\depository\Model\emptyDialog.py
'''
try:
    import maya.cmds as cmds
    import maya.mel as mel
    import maya.OpenMayaUI as omui
    import maya.OpenMaya as om
    import pymel.core.datatypes as dt
    def getMayaWindow():
        ptr = omui.MQtUtil.mainWindow()
        return wrapInstance(long(ptr), qw.QWidget)

    MayaParent = getMayaWindow()
    # maya相关模块加载
except:
    MayaParent = None

from Qt import QtWidgets as qw,QtCore,QtGui,IsPySide,IsPySide2
# 工具架启动的时候会自动载入Qt模块来适配各种Qt版本
# 如需移植Qt模块，请查看Qt.__file__

if IsPySide:
    from shiboken import wrapInstance
elif IsPySide2:
    from shiboken2 import wrapInstance

import sys
import logging

logging.basicConfig(level = logging.INFO,format = '%(levelname)s|%(asctime)s - %(name)s - %(funcName)s : %(message)s')
logger = logging.getLogger('emptyDialog')
logger.setLevel(logging.DEBUG)

class emptyDialog(qw.QMainWindow):

    @property
    def title(self):
        return self._title

    @title.setter
    def title(self,val):
        if isinstance(val,str):
            self._title = val
            self.setWindowTitle(val)

    @property
    def x(self):
        return self._x

    @x.setter
    def x(self,val):
        if isinstance(val,int):
            self._x = val
            self.resize(val,self.y)
    @property
    def y(self):
        return self._y

    @y.setter
    def y(self,val):
        if isinstance(val,int):
            self._y = val
            self.resize(self.x,val)


    def __init__(self,title='default figo widget',parent=None):
        #super(emptyDialog,self).__init__(parent)
        qw.QMainWindow.__init__(self)
        # 2.7和3.0的super写法与工具架不兼容，请使用2.2写法。


        self._title='' 
        self._x=0 
        self._y=0
        self.m_DragPosition=self.pos()
        self.m_drag=False
        # 预设变量

        self.x = 400
        self.y = 300
        # 设置默认窗口大小
        self.move(0,0)
        # 打开窗口后，窗口默认的位置

        self.title = title
        self.initData()
        self.readSettings()
        # 读取窗口设定

        self.setWindowFlags(QtCore.Qt.SubWindow|QtCore.Qt.FramelessWindowHint | QtCore.Qt.WindowStaysOnTopHint)
        # 子窗口，无边框，永远在最上

        self.setMouseTracking(True)
        # 鼠标追踪，适用于无标题栏拖拽


        self.createUI()

    def createSubTitleLine(self,height=25):
        rtLine = qw.QHBoxLayout()
        Xbtn=qw.QPushButton('x')
        Xbtn.setFixedHeight(height)
        Xbtn.setFixedWidth(height)
        rtLine.addWidget(Xbtn)
        rtLine.addStretch()
        
        
        #-----TitleLabel-----------------------
        TitleLab=qw.QLabel('create joint base on curve')
        TitleLab.setFixedHeight(height)
        rtLine.addWidget(TitleLab)
        rtLine.addStretch()

        Xbtn.clicked.connect(self.Cl_Ui)
        return rtLine

    def createUI(self):
        mian=qw.QWidget(self)
        self.setCentralWidget(mian)
        self.mv = qw.QVBoxLayout()
        mian.setLayout(self.mv)
        # 主框架，设置主要在self.mv上
        
        titleLine = self.createSubTitleLine()
        self.mv.addLayout( titleLine )
        # 伪标题栏

        #------main btn-----------------------


        
        self.BtnA1=qw.QPushButton(u'男性底板')
        # button的一些设定
        self.BtnA2=qw.QPushButton(u'女性底板')
        # button的一些设定
        self.BtnA3=qw.QPushButton(u'传递权重并设置')
        # button的一些设定
        self.BtnA4=qw.QPushButton(u'准备手肘')
        # button的一些设定
        self.BtnA5=qw.QPushButton(u'完成手肘')
        # button的一些设定
        self.BtnA6=qw.QPushButton(u'准备膝盖')
        # button的一些设定
        self.BtnA7=qw.QPushButton(u'完成膝盖')
        # button的一些设定
        self.BtnA8=qw.QPushButton(u'最终调整')
        # button的一些设定
        # self.BtnA1=qw.QPushButton('push it')
        # self.BtnA1.setEnabled(True)
        # self.BtnA1.setFixedHeight(20)
        # self.BtnA1.setFixedWidth(60)
        # button的一些设定

        self.mv.addWidget(self.BtnA1)
        self.mv.addWidget(self.BtnA2)
        self.mv.addWidget(self.BtnA3)
        self.mv.addWidget(self.BtnA4)
        self.mv.addWidget(self.BtnA5)
        self.mv.addWidget(self.BtnA6)
        self.mv.addWidget(self.BtnA7)
        self.mv.addWidget(self.BtnA8)
        # 将btn添加到layout中  

        self.BtnA1.clicked.connect(self.Btn1Fn)
        self.BtnA2.clicked.connect(self.Btn2Fn)
        self.BtnA3.clicked.connect(self.Btn3Fn)
        self.BtnA4.clicked.connect(self.Btn4Fn)
        self.BtnA5.clicked.connect(self.Btn5Fn)
        self.BtnA6.clicked.connect(self.Btn6Fn)
        self.BtnA7.clicked.connect(self.Btn7Fn)
        self.BtnA8.clicked.connect(self.Btn8Fn)

        # 给button绑定一个功能，新式写法
        

    # =====================================================================================
    # 以下内容是模板内容，请参考注释酌情修改

    def initData(self):
        self.dataSettings = QtCore.QSettings("SStoolshelf - subwindows", "emptyDialog")
        # 创建和读取qsetting的设置
        # 前一项是组，后一个是项，为了避免串数据，请单独设置组和项的名字。

    def readSettings(self):
        self.dataSettings.beginGroup('SStoolshelf - subwindows - main')
        windowGeometry = self.dataSettings.value('window_geometry')
        windowState = self.dataSettings.value('window_state')
        self.restoreGeometry(windowGeometry)
        self.restoreState(windowState)
        self.dataSettings.endGroup()

    def writeSettings(self):
        self.dataSettings.beginGroup('SStoolshelf - subwindows - main')
        self.dataSettings.setValue('window_geometry', self.saveGeometry())
        self.dataSettings.setValue('window_state', self.saveState())
        self.dataSettings.endGroup()

        
    def mousePressEvent(self,event):
        if event.button() == QtCore.Qt.LeftButton or event.button() == QtCore.Qt.RightButton:
            self.m_drag=True
            self.m_DragPosition=event.globalPos()-self.pos()
            event.accept()
            
    def mouseMoveEvent(self,QMouseEvent):
        if QMouseEvent.buttons() and QtCore.Qt.LeftButton:
            self.move(QMouseEvent.globalPos()-self.m_DragPosition)
            QMouseEvent.accept()
            
    def mouseReleaseEvent(self,QMouseEvent):
        self.m_drag=False

    def closeEvent(self, event):
        self.writeSettings()
        # 写入临时信息，比如位置大小等
        self.deleteLater()  
        # 在窗口关闭的时候触发销毁自己的行为

    # 以上内容是模板内容，请参考注释酌情修改
    # =====================================================================================

    def Btn1Fn(self):

        manTemplate = r"S:\PipeData\pipeTemp\RigTemplate\template\manTemplate.ma"
        cmds.file(manTemplate,i=1,ns='rigtemp')

    def Btn2Fn(self):

        manTemplate = r"S:\PipeData\pipeTemp\RigTemplate\template\womanTemplate.ma"
        cmds.file(manTemplate,i=1,ns='rigtemp')

    def skinPassSel(self,allobj = []):
        # 传递权重的功能，输入两个物体，第一个有权重，第二是目标
        # allobj=cmds.ls(sl=1)
        if(len(allobj)<2):
            cmds.error('please sel two objects.skined first.')
            return False
        else:
            skinO=allobj[0]
            targetO=allobj[1:]
            baseSkinCls=mel.eval('findRelatedSkinCluster("'+skinO+'")')
            if baseSkinCls!='':
                jotSkin=cmds.listConnections(baseSkinCls+'.matrix')
                for t in targetO:
                    targetSkinCls=cmds.skinCluster(jotSkin, t,tsb=True,sm=2)
                    cmds.copySkinWeights( ss=baseSkinCls, ds=targetSkinCls[0], noMirror=True,influenceAssociation='closestJoint',surfaceAssociation='closestPoint')
                    print (skinO+"====skinWeight has passed to===="+t)
            else:
                print 'first object have no skinCluster node.'
                return False
        cmds.select(cl=1)
        return True

    def Btn3Fn(self):
        cmds.addAttr('rigtemp:visable_ctrl',ln="HairStyle",at='long',min=0,max=10,dv=0,k=1)

        targetList = [
            'head_eyelash_mod_00',
            'head_eyelash_mod_01',
            'body_main_mod_00',
            'head_main_mod_00',
            'head_teeth_mod_00',
            'head_teeth_mod_01',
            'body_limb_mod_00',
            'head_eye_mod_00',
            'head_brow_mod_00'
            ]
        for tl in targetList:
            fr = '%s%s' % ('rigtemp:proxy_',tl)
            to = '%s%s' % ('mod:',tl)
            try:
                self.skinPassSel(allobj = [fr,to])
            except:
                print 'fail on ',tl
            
            
        cmds.setAttr('doNotTouch_grp.v',0)

        cmds.blendShape('mod:body_hand_mod_00',n='hans_bs_auto')
        cmds.blendShape('hans_bs_auto',
                                    edit=True,
                                    topologyCheck=False,
                                    target=('mod:body_hand_mod_00', 0, 'rigtemp:proxy_body_hand_mod_00', 1.0)
                                    )
        cmds.setAttr('hans_bs_auto.proxy_body_hand_mod_00',1)

        pDc = {
            'rig_grp':['rigtemp:Group','rigtemp:visable_ctrl_grp','rigtemp:all_facial_ctrl_grp','rigtemp:all_arrow_ctrl_grp'],
            'doNotTouch_grp':['rigtemp:proxy_duplicate_layer_grp','rigtemp:all_facial_jnt_grp']
            
        }

        for key,val in pDc.items():
            for i in val:
                try:
                    cmds.parent(i,key)
                except:
                    pass

        # cmds.parent('rigtemp:Group','rig_grp')
        # cmds.parent('rigtemp:visable_ctrl_grp','rig_grp')
        # cmds.parent('rigtemp:proxy_duplicate_layer_grp','doNotTouch_grp')
        # cmds.parent('rigtemp:all_facial_jnt_grp','doNotTouch_grp')
        # cmds.parent('rigtemp:all_facial_ctrl_grp','rig_grp')

        cmds.setAttr("model_grp.overrideEnabled",1)
        cmds.connectAttr('rigtemp:visable_ctrl.Model_Type','model_grp.overrideDisplayType',f=1)

        diff = ['rigtemp']

        for ns in diff:
            if cmds.namespace( exists=str(ns)):
                cmds.namespace(rm=str(ns),f=1,mergeNamespaceWithParent=1)
                
    def Btn4Fn(self):
        
        sel = cmds.ls(sl=1)[0]
        self.elbowMod = sel
        target_name = 'left'
        temp_duplicate = cmds.duplicate(sel, n='left')[0]
        blendshape_node = cmds.blendShape(sel, frontOfChain=True, n='elbow_fix_bs')[0]

        last_used_index = cmds.blendShape(blendshape_node, q=True, weightCount=True)
        new_target_index = 0 if last_used_index == 0 else last_used_index - 1
        cmds.blendShape(blendshape_node, e=True, target=(sel, new_target_index, target_name, 1.0))
        #cmds.delete(temp_duplicate)
        #cmds.blendShape(blendshape_node, e=True, resetTargetDelta=(0, new_target_index))
        cmds.sculptTarget(blendshape_node, e=True, target=0)
        cmds.setAttr('FKElbow_L.rz',90)
        cmds.setAttr('elbow_fix_bs.left',1)
        cmds.setAttr('left.v',0)
        
    def Btn5Fn(self):
        if cmds.objExists('left'):
            cmds.delete('left')
        if cmds.objExists('right'):
            cmds.delete('right')
            
        # self.elbowMod = 'mod:body_limb_mod_00'
            
        blendshape_node = 'elbow_fix_bs'
        last_used_index = cmds.blendShape(blendshape_node, q=True, weightCount=True)
        new_target_index = 0 if last_used_index == 0 else last_used_index - 1
        duplicated_target_name = cmds.sculptTarget(blendshape_node, e=True, 
            regenerate=True, 
            target=new_target_index
            )[0]
        temp_duplicate = cmds.duplicate(duplicated_target_name, n='right')[0]

        last_used_index = cmds.blendShape(blendshape_node, q=True, weightCount=True)
        #new_target_index = 0 if last_used_index == 0 else last_used_index - 1
        new_target_index = last_used_index
        cmds.blendShape(blendshape_node, e=True, target=(self.elbowMod, new_target_index, temp_duplicate, 1.0))

        cmds.blendShape(blendshape_node, e=True,
                        flipTarget=[(0, new_target_index)], # list of base and target pairs (0=base shape index
                        mirrorDirection=0, # 0=negative,1=positive
                        symmetrySpace=1, # 0=topological, 1=object, 2=UV
                        symmetryAxis='x', # for object symmetrySpace
                        )
        cmds.delete(duplicated_target_name)
        cmds.delete(temp_duplicate)
        cmds.setAttr('FKElbow_L.rz',0)
        cmds.setAttr('FKElbow_R.rz',0)


    def Btn6Fn(self):
        sel = cmds.ls(sl=1)[0]
        self.kneeMod = sel
        target_name = 'left'
        temp_duplicate = cmds.duplicate(sel, n='left')[0]
        blendshape_node = cmds.blendShape(sel, frontOfChain=True, n='knee_fix_bs')[0]

        last_used_index = cmds.blendShape(blendshape_node, q=True, weightCount=True)
        new_target_index = 0 if last_used_index == 0 else last_used_index - 1
        cmds.blendShape(blendshape_node, e=True, target=(sel, new_target_index, target_name, 1.0))
        #cmds.delete(temp_duplicate)
        #cmds.blendShape(blendshape_node, e=True, resetTargetDelta=(0, new_target_index))
        cmds.sculptTarget(blendshape_node, e=True, target=0)
        cmds.setAttr('FKIKLeg_L.FKIKBlend',0)
        cmds.setAttr('FKKnee_L.rz',-90)
        cmds.setAttr('knee_fix_bs.left',1)
        cmds.setAttr('left.v',0)
        
    def Btn7Fn(self):
        if cmds.objExists('left'):
            cmds.delete('left')
        if cmds.objExists('right'):
            cmds.delete('right')
            
        # self.kneeMod = 'mod:body_subadds_mod_02'
        blendshape_node = 'knee_fix_bs'
        last_used_index = cmds.blendShape(blendshape_node, q=True, weightCount=True)
        new_target_index = 0 if last_used_index == 0 else last_used_index - 1
        duplicated_target_name = cmds.sculptTarget(blendshape_node, e=True, 
            regenerate=True, 
            target=new_target_index
            )[0]
        temp_duplicate = cmds.duplicate(duplicated_target_name, n='right')[0]

        last_used_index = cmds.blendShape(blendshape_node, q=True, weightCount=True)
        #new_target_index = 0 if last_used_index == 0 else last_used_index - 1
        new_target_index = last_used_index
        cmds.blendShape(blendshape_node, e=True, target=(self.kneeMod, new_target_index, temp_duplicate, 1.0))

        cmds.blendShape(blendshape_node, e=True,
                        flipTarget=[(0, new_target_index)], # list of base and target pairs (0=base shape index
                        mirrorDirection=0, # 0=negative,1=positive
                        symmetrySpace=1, # 0=topological, 1=object, 2=UV
                        symmetryAxis='x', # for object symmetrySpace
                        )
        cmds.delete(duplicated_target_name)
        cmds.delete(temp_duplicate)
        cmds.setAttr('FKIKLeg_L.FKIKBlend',10)
        cmds.setAttr('FKKnee_L.rz',0)
        
    def Btn8Fn(self):
        cmds.connectAttr('body_bs.left','knee_fix_bs.left',f=1)
        cmds.connectAttr('body_bs.right','knee_fix_bs.right',f=1)

                
                
        cmds.connectAttr('arm_bs.left','elbow_fix_bs.left',f=1)
        cmds.connectAttr('arm_bs.right','elbow_fix_bs.right',f=1)


        cmds.setAttr("visable_ctrl.Model_Type",2)

    def Op_Ui(self):
        # 显示接口
        self.show()
        
    def Cl_Ui(self):
        # 关闭接口
        self.close()
global titleVar
titleVar = emptyDialog()
titleVar.Op_Ui()






