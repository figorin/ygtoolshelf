'''
@Date: 2020-04-25 22:29:07
LastEditors: figo - uz
LastEditTime: 2020-12-24 14:31:43
FilePath: \undefinedc:\gitlab\sstoolshelf\debug\FgToolshelf\depository\Rig\openPublishDialog_rig.py
'''
import sys
import os
try:
    toolPath = os.environ['FGPIPEROOT']
except KeyError:
    # toolPath = r'S:\PipeProgram\figoPipe\pipelineMainProgram'
    toolPath = r'C:\gitLab\brownser01'
finally:
    if toolPath not in sys.path:
        sys.path.append(toolPath)

import figoBrownser.platforms.maya.rig.browser_Rig_Publish as Rig_Pub
reload(Rig_Pub)

global FGbrownserWindow
FGbrownserWindow = None
FGbrownserWindow = Rig_Pub.main()