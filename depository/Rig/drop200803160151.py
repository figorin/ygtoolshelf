import maya.cmds as cmds

FKL = 'FKShoulder_L'
FKR = 'FKShoulder_R'

LrotZero = cmds.group(empty=1,n='GlobalShoulder_L')
LrotMove = cmds.group(empty=1,n='GlobalOffsetShoulder_L')

RrotZero = cmds.group(empty=1,n='GlobalShoulder_R')
RrotMove = cmds.group(empty=1,n='GlobalOffsetShoulder_R')

cmds.parent(RrotZero,RrotMove)
cmds.parent(LrotZero,LrotMove)

Lmat = cmds.xform(FKL,q=1,ws=1,m=1)
cmds.xform(LrotMove,ws=1,m=Lmat)

Rmat = cmds.xform(FKR,q=1,ws=1,m=1)
cmds.xform(RrotMove,ws=1,m=Rmat)

cmds.parent(RrotMove,'GlobalSystem')
cmds.parent(LrotMove,'GlobalSystem')

consL = cmds.orientConstraint(LrotZero,'FKExtraShoulder_L',offset=[0,0,0],weight=1)
attrL = '%s.%sW0' % (consL[0],LrotZero)
consR = cmds.orientConstraint(RrotZero,'FKExtraShoulder_R',offset=[0,0,0],weight=1)
attrR = '%s.%sW0' % (consR[0],RrotZero)

cmds.addAttr(FKL,ln="Global",at='double',min=0,max=1,dv=0,k=1)
cmds.addAttr(FKR,ln="Global",at='double',min=0,max=1,dv=0,k=1)

cmds.connectAttr('%s.Global'%FKR, attrR,f=1)
cmds.connectAttr('%s.Global'%FKL, attrL,f=1)