import maya.cmds as cmds
import maya.OpenMaya as om
import copy
import maya.mel as mel
from Qt import QtWidgets as qw, QtGui, QtCore, IsPySide, IsPySide2

'''
bsName = 'blendShape1'

#bsName += '.weight[1]'

bsName += '.inputTarget'

bsName += '[0].inputTargetGroup'

bsName += '[0].inputTargetItem'

bsName += '[6000].inputGeomTarget'

gg = cmds.listAttr(bsName, sn=1)

for g in gg:
    print g



# get the quantity of the blendshape targets.
bsName = 'blendShape1'
bsName += '.inputTarget'

bsName += '[0].inputTargetGroup'
ff = cmds.getAttr(bsName,mi=1)
'''


# get the inner parts of blendshape targets.

class transferBlendshape_Class():

    def getGeoNameFromNode(self,blendShapeNode=None):

        hist = cmds.listHistory(blendShapeNode, f=True)

        shapeGeo = cmds.ls(hist, type='mesh')[0]

        geoName = cmds.listRelatives(shapeGeo, ap=True)[0]

        shapeGeoSplit = shapeGeo.split('|')[-1]

        if str(shapeGeoSplit) == str(geoName):
            cmds.rename(shapeGeo, str(shapeGeoSplit) + 'Shape')

        return geoName

    def extrudeBlendshapeTarget(self,bsName, correctiveGroup, correctiveItem='6000'):


        ff = cmds.sculptTarget(bsName, e=True, regenerate=True, target=correctiveGroup)

        geo = self.getGeoNameFromNode(bsName)

        wt = '.weight[%s]' % correctiveGroup

        tarObjName = cmds.listAttr(bsName + wt, sn=1)[0]

        newName = geo + '_' + tarObjName

        cmds.rename(ff[0], newName)

        om.MGlobal.displayInfo('done')

        return {'meshName': newName, 'bsAttr': tarObjName}



    def getPoints(self, geo, isList=False):
        sel = om.MSelectionList()
        dag = om.MDagPath()

        sel.add(geo)
        sel.getDagPath(0, dag)

        mesh = om.MFnMesh(dag)

        vts = om.MPointArray()
        mesh.getPoints(vts, om.MSpace.kObject)

        if isList:
            theList = []
            for i in range(vts.length()):
                tempA = [vts[i].x, vts[i].y, vts[i].z]
                theList.append(tempA)
            return theList
        else:
            return vts


    def setPoints(self, inputMod, inputAPIarray):
        sel = om.MSelectionList()
        dag = om.MDagPath()

        sel.add(inputMod)
        sel.getDagPath(0, dag)

        mesh = om.MFnMesh(dag)

        # vts = om.MPointArray()
        mesh.setPoints(inputAPIarray, om.MSpace.kObject)


    def getMuteList(self, orig, mute, isDict=True, oType='relative',rVal = 4):
        oo = []
        xx = []
        if isinstance(orig,list):
            oo = list(orig)
        else:
            oo = self.getPoints(orig, isList=True)

        if isinstance(mute,list):
            xx = list(mute)
        else:
            xx = self.getPoints(mute, isList=True)

        #print 'oo :'
        #print oo
        #print 'xx :'
        #print xx

        relativeGatherArray = []

        for i in range(len(xx)):
            gx = round(xx[i][0] - oo[i][0],rVal)

            gy = round(xx[i][1] - oo[i][1],rVal)

            gz = round(xx[i][2] - oo[i][2],rVal)

            gather = [gx, gy, gz]

            relativeGatherArray.append(gather)

        absoluteGatherArray = []

        absoluteGatherArray = list(xx)

        if isDict:
            relativeGatherDict = {}

            absoluteGatherDict = {}


            for count, val in enumerate(relativeGatherArray):
                isSkip = False
                thex = -0.0001 < val[0] <0.0001
                they = -0.0001 < val[1] <0.0001
                thez = -0.0001 < val[2] <0.0001
                if thex and they and thez:
                    isSkip = True

                if isSkip:
                    #print 'found'
                    #print ''
                    continue
                else:
                    #if val != [0.0, 0.0, 0.0]:

                    #print count
                    #print val
                    #print ''
                    relativeGatherDict.update({count: val})
                    absoluteGatherDict.update({count: absoluteGatherArray[count]})

            if oType == 'relative':
                return relativeGatherDict
            elif oType == 'absolute':
                return absoluteGatherDict

        else:
            print 'return gather array'

            if oType == 'relative':
                return relativeGatherArray
            elif oType == 'absolute':
                return absoluteGatherArray


    def getBlendShapeData(self, ogMod, muteModList):
        gData = {}
        for a in muteModList:
            temp = self.getMuteList(a, ogMod, isDict=True)
            gData.update({a: temp})
        return gData


    def rebuildBlendTargetsByDict(self, modName, dictName, prefix='paste', direct = '+', oType='relative', scaleArray = [1.0, 1.0, 1.0]):
        muteDataOrig = self.getPoints(modName, isList=True)

        dupMods = []
        for key, item in dictName.items():
            dupTemp = cmds.duplicate(modName, rr=1)
            newName = prefix + '_' + str(key)
            cmds.rename(dupTemp[0], newName)
            dupMods.append(newName)

            muteData = copy.deepcopy(muteDataOrig)
            mpArray = om.MPointArray()
            for i in range(len(muteData)):

                if i in item.keys():
                    if oType == 'relative':
                        if direct == '-':
                            muteData[i][0] -= (float(item[i][0]) * scaleArray[0])
                            muteData[i][1] -= (float(item[i][1]) * scaleArray[1])
                            muteData[i][2] -= (float(item[i][2]) * scaleArray[2])
                        else:
                            muteData[i][0] += (float(item[i][0]) * scaleArray[0])
                            muteData[i][1] += (float(item[i][1]) * scaleArray[1])
                            muteData[i][2] += (float(item[i][2]) * scaleArray[2])
                    else:
                        muteData[i][0] = float(item[i][0])
                        muteData[i][1] = float(item[i][1])
                        muteData[i][2] = float(item[i][2])


                omp = om.MPoint()
                omp.x = muteData[i][0]
                omp.y = muteData[i][1]
                omp.z = muteData[i][2]
                mpArray.append(omp)
            #print '---'
            #print newName
            #print type(newName)
            self.setPoints(newName, mpArray)
        return dupMods

    def addBlendTargetsToNode(self, modName, targetsArray,cutInitail = True, autoFn = 'none'):
        isBSexists = cmds.ls(*cmds.listHistory(modName) or [], type='blendShape')
        if isBSexists:
            bsNode = isBSexists[0]
            bsAttrList = cmds.listAttr(bsNode + '.weight', m=1)
            bsAttrDict = dict(zip(bsAttrList, range(len(bsAttrList))))

            newNameArray = []
            for oldCName in targetsArray:
                newCName = ''
                if cutInitail:
                    temp = oldCName.split('_')

                    newCName = '_'.join(temp[1:])
                else:
                    newCName = oldCName
                newNameArray.append(newCName)

            realBsNodes = list(targetsArray)
            for nna,ta,count in zip(newNameArray,targetsArray,range(len(targetsArray))):
                checkStr = '%s.%s' % (bsNode,nna)
                chk = cmds.objExists(checkStr)
                if chk:
                    reply = qw.QMessageBox()
                    reply.setText(checkStr + ' already exists. do you want...?')
                    reply.setWindowTitle('found exists')
                    reply.addButton('replace', qw.QMessageBox.YesRole)
                    reply.addButton('add', qw.QMessageBox.NoRole)
                    reply.addButton('skip', qw.QMessageBox.RejectRole)
                    reply.exec_()

                    sel = reply.clickedButton()
                    eChk = ''
                    if sel.text() == 'replace':
                        cmds.blendShape(bsNode, edit=True, t=(modName, bsAttrDict[nna], ta, 1.0))
                        cmds.aliasAttr(nna, bsNode + '.' + ta)
                    elif sel.text() == 'add':
                        # default function is 'add'
                        nowCt = cmds.blendShape(bsNode, q=1, weightCount=1)
                        uniName = 'paste_' + nna
                        for i in range(10):
                            # checkName = str(uniName)
                            if cmds.objExists(bsNode + '.' + uniName):
                                uniName = 'paste_' + uniName
                            else:
                                break
                        cmds.rename(ta, uniName)
                        cmds.blendShape(bsNode, edit=True, t=(modName, nowCt, uniName, 1.0))
                        realBsNodes[count] = uniName
                    else:
                        continue

                else:
                    nowCt = cmds.blendShape(bsNode, q=1, weightCount=1)

                    cmds.blendShape(bsNode, edit=True, t=(modName, nowCt, ta, 1.0))

                    cmds.aliasAttr(nna, bsNode + '.' + ta)

            #return realBsNodes

            return [bsNode,realBsNodes]
        else:

            newBsName = modName + '_bs'

            bsNode = cmds.blendShape(targetsArray, modName, n=newBsName)

            if cutInitail:

                for oldCName in targetsArray:

                    temp = oldCName.split('_')

                    newCName = '_'.join(temp[1:])

                    cmds.aliasAttr(newCName, newBsName + '.' + oldCName)

            return [bsNode,targetsArray]


    def alignSelection(self, selections):
        targetNode = selections[-1]
        moveNodes = selections[:-1]

        searchAttr = ['tx', 'ty', 'tz', 'rx', 'ry', 'rz']
        targetAttr = []
        for sa in searchAttr:
            val = cmds.getAttr('%s.%s' % (targetNode, sa))
            targetAttr.append(val)

        for mn in moveNodes:
            for sa, ta in zip(searchAttr, targetAttr):
                cmds.setAttr('%s.%s' % (mn, sa), ta)


    def createWrap(self, theMesh, theMeshBs):
        cmds.select(theMesh)
        cmds.select(theMeshBs, add=1)

        beforeCreate = cmds.ls(type='wrap')

        ff = mel.eval('CreateWrap;')

        wrapBaseNode = theMeshBs + 'Base'

        afterCreate = cmds.ls(type='wrap')

        newWrap = ''
        for ac in afterCreate:
            if ac not in beforeCreate:
                newWrap = ac
                break
        return [newWrap, wrapBaseNode]


transClass = transferBlendshape_Class()

correctiveGroup = 1

correctiveItem = '6000'

bsName = 'blendShape1'

# ------ input over -------

# extrudeBlendshapeTarget(bsName, correctiveGroup)

# originalShape(bsName)

# gg = getMuteList(transMesh,'gg',isDict=True)
# print gg

'''
ptDict = {}
tempMeshs = []
for i in range(3):
    shapeMesh = loadClass.extrudeBlendshapeTarget('blendShape1',i)
    tempMeshs.append(shapeMesh['meshName'])
    ptList = loadClass.getPoints(shapeMesh['meshName'],isList=True)
    ptDict.update({shapeMesh['bsAttr']: ptList})
cmds.delete(tempMeshs)

print ptDict


# ptList = loadClass.getPoints('gg',isList=True)
'''
# loadClass.extrudeBlendshapeTarget('blendShape1',3)

theA = ['pSphere2', 'pSphere3', 'pSphere4', 'pSphere5']
ogMod = 'gg'

# bsData = getBlendShapeData(ogMod, theA)
# get all transform data ------ copy action

#print bsData


# muteMod1 = 'gg2'



bsTargsArray = ['gg2_pSphere3', 'gg2_pSphere4']

newModName = 'gg2'

#addBlendTargetsToNode(newModName, bsTargsArray)




