import sys

from Qt import QtWidgets as qw, QtGui, QtCore, IsPySide, IsPySide2

if IsPySide:
    from shiboken import wrapInstance
elif IsPySide2:
    from shiboken2 import wrapInstance

try:
    import maya.OpenMayaUI as omui
    import maya.OpenMaya as om
    import pymel.core as pm
    import pymel.core.datatypes as dt
    import maya.cmds as cmds
    import maya.mel as mel
    import sys

    def getMayaWindow():
        ptr = omui.MQtUtil.mainWindow()
        return wrapInstance(long(ptr), qw.QWidget)

    MayaParent = getMayaWindow()
except:
    pass

import copy
import redBlackStyleSheet as MstyleTemp
Mstyle = MstyleTemp.RedBlackStyleSheet()

import mirrorFunctions as mirrorFns
fgMirrorClass = mirrorFns.mirrorVert_Class()


import jsonBaseClass
figoJson = jsonBaseClass.jsonBaseClass()


import transFunctions as transFns
transClass = transFns.transferBlendshape_Class()
#reload(jsonBaseClass)
#fgMirrorClass = mirrorVert_Class()

#fgMirrorClass.loadSourceModel('gg')

class transShapeUIClass(qw.QMainWindow):
    uniqueInstance = None
    versionCode = 'ver' + ' 0.1.0'
    WINDOW_OBJECT_NAME = 'transfer_Blendshape'
    HOLY_ID = 0

    def __init__(self, parent=None):
        # ----- formular -----
        super(transShapeUIClass, self).__init__(MayaParent)

        self.resize(200, 150)
        self.parent = MayaParent
        self.setObjectName(self.WINDOW_OBJECT_NAME)
        self.setDocumentMode(False)
        self.initData()
        self.readSettings()
        self.refreshCallCount()
        # ----- formular end -----

        # ----- custom Attr ------
        self.create_ui()
        self.setAttribute(QtCore.Qt.WA_DeleteOnClose)

        self.mirrorAxis = 0
        # mirror functions attr.0 == x,1 == y,2 == z.

        self.theFrom = None
        self.theTo = None
        self.blendshapeNode = ''
        self.selID = []
        self.copyDict = {}

        self.tempFrom = ''
        self.nowTab = 0

    def create_ui(self):
        self.setParent(MayaParent)
        self.setWindowFlags(QtCore.Qt.Window)
        self.setWindowTitle(self.WINDOW_OBJECT_NAME)

        self.setDockOptions(qw.QMainWindow.AllowTabbedDocks | qw.QMainWindow.AnimatedDocks)
        self.setUnifiedTitleAndToolBarOnMac(False)
        self.setAttribute(QtCore.Qt.WA_DeleteOnClose)

        mainWidget = qw.QWidget()
        self.setCentralWidget(mainWidget)
        mainWidget.setAttribute(QtCore.Qt.WA_StyledBackground)

        vlay = qw.QVBoxLayout()
        mainWidget.setLayout(vlay)

        line1 = qw.QHBoxLayout()
        self.pushOrig = qw.QPushButton('pick\norg')
        self.pushOrig.setToolTip('orig')
        self.pushOrig.setStyleSheet(Mstyle.QPushButton(kw='off'))
        self.pushOrig.setFixedSize(48, 48)

        self.pushTarg = qw.QPushButton('pick\nTar')
        self.pushTarg.setToolTip('targ')
        self.pushTarg.setStyleSheet(Mstyle.QPushButton(kw='off'))
        self.pushTarg.setFixedSize(48, 48)

        line1_v2 = qw.QVBoxLayout()
        line1_v2_h1 = qw.QHBoxLayout()
        line1_v2_h2 = qw.QHBoxLayout()

        self.txtOrig = qw.QLineEdit('nothing')
        self.txtOrig.setEnabled(False)
        self.txtOrig.setStyleSheet(Mstyle.QLineEdit(kw='c', fontSize='15px'))
        self.txtOrig.setMinimumWidth(80)

        self.txtTarg = qw.QLineEdit('nothing')
        self.txtTarg.setAlignment(QtCore.Qt.AlignRight)
        self.txtTarg.setEnabled(False)
        self.txtTarg.setStyleSheet(Mstyle.QLineEdit(kw='c', fontSize='15px'))
        self.txtTarg.setMinimumWidth(80)

        line1_v2_h1.addWidget(self.txtOrig)
        line1_v2_h1.addStretch(1)
        line1_v2_h2.addStretch(1)
        line1_v2_h2.addWidget(self.txtTarg)
        line1_v2.addLayout(line1_v2_h1)
        line1_v2.addLayout(line1_v2_h2)

        line1.addWidget(self.pushOrig)
        line1.addLayout(line1_v2)
        line1.addWidget(self.pushTarg)

        vlay.addLayout(line1)
        #================================

        tabWidgetStyle = ('QTabWidget::pane             { border-top: 0px solid #C2C7CB;} '
                          'QTabWidget::tab-bar          { top: 10px; }'
                          'QTabBar::tab                 { color:rgb(150,150,150);background:transparent; border: 0px solid #C4C4C3;  min-height: 58px; min-width: 18px; border-bottom-color: #C2C7CB;border-top-left-radius: 8px;  border-bottom-left-radius: 8px;   padding: 0px; }'
                          'QTabBar::tab:hover           { color:rgb(255,255,255);background: transparent;  border: 1px solid #C4C4C3;border-right-color: transparent; padding: 0px; }'
                          'QTabBar::tab:selected        { background: rgb(109,109,109);  border-color: #9B9B9B; border-bottom-color: #C2C7CB;border-right-color: rgb(109,109,109);margin-left: 0px; margin-right: 0px;}  '
                          'QTabBar::tab:!selected       { margin-top: 0px; }'
                          'QTabBar::tab:first:hover     { color:rgb(130,255,202);background:transparent;margin-left: 0;border: 1px solid rgb(130,255,202);border-right-color: rgb(109,109,109);} '
                          'QTabBar::tab:first:selected  { background:rgb(109,109,109);margin-left: 0;} '
                          'QTabBar::tab:last:hover      { color:rgb(253,202,137);background:transparent;margin-right: 0;border: 1px solid rgb(253,202,137);border-right-color: rgb(109,109,109);} '
                          'QTabBar::tab:last:selected   { background:rgb(109,109,109);margin-right: 0;} '

                          'QTabBar::tab:only-one        { margin: 0;} ')

        theTab = qw.QTabWidget()
        theTab.setTabPosition(theTab.West)
        theTab.setStyleSheet(tabWidgetStyle)
        vlay.addWidget(theTab)

        # ============================
        type1Grp = qw.QGroupBox()
        type1Grp.setStyleSheet(Mstyle.QGroupBox(kw='a', borderRad=[0, 10, 10, 10]))

        t1v = qw.QVBoxLayout()
        t1h1 = qw.QHBoxLayout()
        t1h2 = qw.QHBoxLayout()
        t1v.addStretch(1)
        t1v.addLayout(t1h1)
        t1v.addStretch(1)
        t1v.addLayout(t1h2)
        t1v.addStretch(1)
        type1Grp.setLayout(t1v)

        t1h1.addStretch(1)
        pushLtoR = qw.QPushButton('<--|--')
        pushLtoR.setToolTip('ltor')
        pushLtoR.setStyleSheet(Mstyle.QPushButton())
        pushLtoR.setFixedSize(64, 64)
        t1h1.addWidget(pushLtoR)

        t1h1.addStretch(1)
        pushRtoL = qw.QPushButton('--|-->')
        pushRtoL.setToolTip('rtol')
        pushRtoL.setStyleSheet(Mstyle.QPushButton())
        pushRtoL.setFixedSize(64, 64)
        t1h1.addWidget(pushRtoL)
        t1h1.addStretch(1)

        t1h2.addStretch(1)
        pushMirror = qw.QPushButton('-->|<--')
        pushMirror.setToolTip('mirror')
        pushMirror.setStyleSheet(Mstyle.QPushButton())
        pushMirror.setFixedSize(64, 64)
        t1h2.addWidget(pushMirror)

        t1h2.addStretch(1)

        lilVb = qw.QVBoxLayout()
        pushReset = qw.QPushButton('reset')
        pushReset.setFixedSize(64, 32)

        spinVal = qw.QSpinBox()
        spinVal.setSuffix('%')
        spinVal.setAlignment(QtCore.Qt.AlignRight)
        spinVal.setRange(0, 100)
        spinVal.setValue(100)
        spinVal.setStyleSheet(Mstyle.QSpinBox() + 'QSpinBox {padding-left:10px;}')
        spinVal.setFixedWidth(64)

        lilVb.addStretch(1)
        lilVb.addWidget(pushReset)
        lilVb.addWidget(spinVal)
        lilVb.addStretch(1)

        t1h2.addLayout(lilVb)
        t1h2.addStretch(1)

        theTab.addTab(type1Grp, 'mirror')

        # -------------------------------
        type2Grp = qw.QGroupBox()
        type2Grp.setStyleSheet(Mstyle.QGroupBox(kw='a'))

        t2v = qw.QVBoxLayout()
        type2Grp.setLayout(t2v)

        # --------------------
        self.radGrp = qw.QButtonGroup()
        self.radGrp.setExclusive(True)

        addStyle = 'QPushButton:checked {border: 2px solid rgba(210, 210, 210, 255);color: rgb(210, 210, 210);}'

        pushRadioA = qw.QPushButton('relative')
        pushRadioA.setCheckable(True)
        pushRadioA.setFixedHeight(20)
        self.radGrp.addButton(pushRadioA, 0)
        pushRadioA.setChecked(True)
        pushRadioA.setStyleSheet(Mstyle.QPushButton(kw='radiobutton') )

        pushRadioB = qw.QPushButton('absolute')
        pushRadioB.setCheckable(True)
        pushRadioB.setFixedHeight(20)
        self.radGrp.addButton(pushRadioB, 1)
        pushRadioB.setStyleSheet(Mstyle.QPushButton(kw='radiobutton') )

        #mfTab = qw.QTabWidget()

        mfp1 = qw.QGroupBox()
        mfp1v1 = qw.QVBoxLayout()
        mfp1v1h1 = qw.QHBoxLayout()
        mfp1v1h2 = qw.QHBoxLayout()

        mfp1.setLayout(mfp1v1)

        pushRefreshBs = qw.QPushButton('Refresh')
        pushRefreshBs.setStyleSheet(Mstyle.QPushButton())

        pushSelAllItm = qw.QPushButton('sel all = ctrl + a')
        pushSelAllItm.setStyleSheet('QPushButton {background-color:transparent;border:0px;}')
        pushSelAllItm.setEnabled(False)

        mfp1v1h1.addWidget(pushRefreshBs)
        mfp1v1h1.addWidget(pushSelAllItm)
        mfp1v1.addLayout(mfp1v1h1)

        self.bsList = qw.QListWidget()
        self.bsList.setStyleSheet(Mstyle.QListWidget(fontSize='15px'))
        self.bsList.setSelectionMode(qw.QAbstractItemView.ExtendedSelection)

        mfp1v1.addWidget(self.bsList)

        pushLoadFromtFile = qw.QPushButton('Load')
        pushLoadFromtFile.setFixedWidth(35)

        pushSavetoFile = qw.QPushButton('Save')
        pushSavetoFile.setFixedWidth(35)

        pushCopyAction = qw.QPushButton('Copy')

        mfp1v1h2.addWidget(pushCopyAction)
        mfp1v1h2.addWidget(pushLoadFromtFile)
        mfp1v1h2.addWidget(pushSavetoFile)
        mfp1v1.addLayout(mfp1v1h2)

        t2vh1 = qw.QHBoxLayout()
        t2vh1.addWidget(pushRadioA)
        t2vh1.addWidget(pushRadioB)
        t2v.addLayout(t2vh1)
        t2v.addWidget(mfp1)

        valScaleBox = qw.QGroupBox('value scale')
        valHBox = qw.QHBoxLayout()
        valScaleBox.setLayout(valHBox)

        vs1Lab = qw.QLabel('x:')
        vs1Lab.setFixedWidth(20)
        self.vs1Spin = qw.QDoubleSpinBox()
        self.vs1Spin.setStyleSheet(Mstyle.QSpinBox())
        self.vs1Spin.setRange(-1000.0, 1000.0)
        self.vs1Spin.setValue(1.0)
        valHBox.addWidget(vs1Lab)
        valHBox.addWidget(self.vs1Spin)

        vs2Lab = qw.QLabel('y:')
        vs2Lab.setFixedWidth(20)
        self.vs2Spin = qw.QDoubleSpinBox()
        self.vs2Spin.setStyleSheet(Mstyle.QSpinBox())
        self.vs2Spin.setRange(-1000.0, 1000.0)
        self.vs2Spin.setValue(1.0)
        valHBox.addWidget(vs2Lab)
        valHBox.addWidget(self.vs2Spin)

        vs3Lab = qw.QLabel('z:')
        vs3Lab.setFixedWidth(20)
        self.vs3Spin = qw.QDoubleSpinBox()
        self.vs3Spin.setStyleSheet(Mstyle.QSpinBox())
        self.vs3Spin.setRange(-1000.0, 1000.0)
        self.vs3Spin.setValue(1.0)
        valHBox.addWidget(vs3Lab)
        valHBox.addWidget(self.vs3Spin)
        t2v.addWidget(valScaleBox)

        pushPasteAction = qw.QPushButton('paste')
        pushPasteAction.setStyleSheet(Mstyle.QPushButton())
        t2v.addWidget(pushPasteAction)

        theTab.addTab(type2Grp, 'copy')

        # --------------------

        type3Grp = qw.QGroupBox()
        type3Grp.setStyleSheet(Mstyle.QGroupBox(kw='a'))

        t3v = qw.QVBoxLayout()
        type3Grp.setLayout(t3v)

        mfp3 = qw.QGroupBox()
        mfp3v1 = qw.QVBoxLayout()
        mfp3v1h1 = qw.QHBoxLayout()
        #mfp3v1h2 = qw.QHBoxLayout()

        mfp3.setLayout(mfp3v1)

        pushRefreshBsp3 = qw.QPushButton('Refresh')
        pushRefreshBsp3.setStyleSheet(Mstyle.QPushButton())

        pushAlignAction = qw.QPushButton('Align')
        pushAlignAction.setStyleSheet(Mstyle.QPushButton())
        pushAlignAction.setFixedWidth(90)

        mfp3v1h1.addWidget(pushRefreshBsp3)
        mfp3v1h1.addWidget(pushAlignAction)
        mfp3v1.addLayout(mfp3v1h1)

        self.bsListp3 = qw.QListWidget()
        self.bsListp3.setStyleSheet(Mstyle.QListWidget(fontSize='15px'))
        self.bsListp3.setSelectionMode(qw.QAbstractItemView.ExtendedSelection)

        mfp3v1.addWidget(self.bsListp3)



        #pushCopyActionp3 = qw.QPushButton('trans data')

        #mfp3v1h2.addWidget(pushCopyActionp3)
        #mfp3v1h2.addWidget(pushLoadFromtFile)
        #mfp3v1.addLayout(mfp3v1h2)

        t3v.addLayout(t2vh1)
        t3v.addWidget(mfp3)

        pushTransAction = qw.QPushButton('trans')
        pushTransAction.setStyleSheet(Mstyle.QPushButton())
        t3v.addWidget(pushTransAction)

        theTab.addTab(type3Grp, 'warp')



        pushRefreshBs.clicked.connect(self.refreshNodeBs)

        self.pushOrig.clicked.connect(self.pickItem)
        self.pushTarg.clicked.connect(self.pickItem)

        pushLtoR.clicked.connect(self.mirrorActionFn)
        pushRtoL.clicked.connect(self.mirrorActionFn)
        pushMirror.clicked.connect(self.mirrorActionFn)

        pushCopyAction.clicked.connect(self.copyActionFn)
        pushPasteAction.clicked.connect(self.pasteActionFn)

        pushSavetoFile.clicked.connect(self.saveToFileFn)
        pushLoadFromtFile.clicked.connect(self.loadFromFileFn)

        pushAlignAction.clicked.connect(self.alignSelectionFn)
        pushTransAction.clicked.connect(self.transBlendshapeToAnotherMod)

        pushRefreshBsp3.clicked.connect(self.refreshTransBs)

        theTab.currentChanged.connect(self.tabChangeFn)

    def tabChangeFn(self,val):
        self.nowTab = val
        print self.nowTab

    def transBlendshapeToAnotherMod(self):
        if not self.bsListp3.selectedItems():
            return False

        self.progress = qw.QProgressDialog()
        self.progress.show()

        self.progress.setMaximum(9)
        self.progress.setLabelText('start trans,this function will be very slow\nplease wait...')
        self.progress.setValue(0)


        self.selID = []
        for si in self.bsListp3.selectedItems():
            self.selID.append(int(si.toolTip()))

        orig = self.theFrom

        if len(self.theTo) > 1:
            om.MGlobal.displayInfo('too many items in pick list.just use the first one')

        pick = self.theTo[0]

        selShape = cmds.listRelatives(orig, s=True)
        lsConnBs = cmds.listConnections(selShape[0] + '.inMesh', s=True)
        if not lsConnBs:
            om.MGlobal.displayInfo('no blendshape in from node.')
            return False

        theBsNode = lsConnBs[0]
        self.progress.setValue(1)
        dupOrig = cmds.duplicate(orig, n=orig + '_dup')[0]
        self.progress.setValue(2)
        dupPick = cmds.duplicate(pick, n=pick + '_dup')[0]
        self.progress.setLabelText('extrude orignal shapes\nplease wait...')
        self.progress.setValue(3)
        bsTarsArray = []
        for i in self.selID:
            exrDict = transClass.extrudeBlendshapeTarget(theBsNode, i)
            bsTargetMesh = exrDict['meshName']
            bsTarsArray.append(bsTargetMesh)
        self.progress.setLabelText('create instance\nplease wait...')
        self.progress.setValue(4)
        origTempBs = cmds.blendShape(bsTarsArray, dupOrig)[0]

        cmds.delete(bsTarsArray)

        wrapRtn = transClass.createWrap(dupPick, dupOrig)

        bsCrtvs = cmds.listAttr(origTempBs + '.weight', m=1)

        pointDatas = {}

        self.progress.setLabelText('rebuilding\nplease wait...')
        self.progress.setValue(5)
        for bc in bsCrtvs:
            cmds.setAttr(origTempBs + '.' + bc, 1)

            pd = transClass.getMuteList(pick, dupPick)

            pointDatas.update({bc: pd})

            cmds.setAttr(origTempBs + '.' + bc, 0)

        cmds.delete([dupOrig, dupPick])

        #for key, val in pointDatas.items():
        self.progress.setLabelText('rebuilding.\nplease wait...')
        self.progress.setValue(6)
        newMods = transClass.rebuildBlendTargetsByDict(pick,pointDatas)
        self.progress.setLabelText('rebuilding..\nplease wait...')
        self.progress.setValue(7)
        gets = transClass.addBlendTargetsToNode(pick,newMods)[1]
        self.progress.setLabelText('rebuilding...\nplease wait...')
        self.progress.setValue(8)
        bigArray = newMods + gets
        for ba in bigArray:
            if cmds.objExists(ba):
                cmds.delete(ba)
        self.progress.setLabelText('done')
        self.progress.setValue(9)
        self.progress.close()



    def alignSelectionFn(self):
        sel = cmds.ls(sl=1)
        if sel:
            transClass.alignSelection(sel)

    def loadFromFileFn(self):
        # when the dict load from file.the sub dict keys will change the type,from int type to string type.
        # this is the bug of json.so we need fix the problem.

        theDialog = qw.QFileDialog.getOpenFileName(self,
                                                   'load blendshape json file',
                                                   'd:/',
                                                   'json File(*.json)')

        thePath = theDialog[0]
        tempDict = figoJson.loadJson(thePath)
        self.copyDict = {}
        if tempDict:
            self.bsList.clear()
            for count, bf in enumerate(tempDict.keys()):
                itm = qw.QListWidgetItem()
                itm.setText(bf)
                itm.setToolTip(str(count))
                self.bsList.addItem(itm)

                # fix type begin
                subDict = tempDict[bf]
                subTemp = {}
                for key, val in subDict.items():
                    subTemp.update({int(key): val})

                self.copyDict.update({bf: subTemp})
                # fix type done

        om.MGlobal.displayInfo('save blendshape as json at : ' + thePath)

    def saveToFileFn(self):

        if self.copyDict:

            theDialog = qw.QFileDialog.getSaveFileName(self,
                                                       'save json file',
                                                       'd:/',
                                                       'json File(*.json)')
            thePath = theDialog[0]
            figoJson.saveJson(thePath, self.copyDict)

            om.MGlobal.displayInfo('save blendshape as json at : ' + thePath)
        else:
            om.MGlobal.displayWarning('no data or inforomation included')


    def pasteActionFn(self):
        # print self.copyDict
        # print type(self.copyDict)
        # print '------'
        if self.copyDict and self.theTo and self.selID:
            if isinstance(self.theTo, list):
                theDict = {}
                oTypeKey = 'relative'
                typeSel = self.radGrp.checkedId()
                if typeSel == 1:
                    oTypeKey = 'absolute'

                useDict = {}
                for key, val in self.copyDict.items():
                    if key in self.selID:
                        useDict.update({key: val})
                # self.copyDict

                scaleArray = [self.vs1Spin.value(), self.vs2Spin.value(), self.vs3Spin.value()]
                for t in self.theTo:
                    #isBSexists = cmds.ls(*cmds.listHistory(t) or [], type='blendShape')

                    dupTargs = transClass.rebuildBlendTargetsByDict(t, useDict, oType=oTypeKey, scaleArray=scaleArray)

                    collectUni = transClass.addBlendTargetsToNode(t, dupTargs, autoFn='none')[1]

                    bigArray = collectUni + dupTargs
                    for ba in bigArray:
                        if cmds.objExists(ba):
                            cmds.delete(ba)
                            #print ba

                om.MGlobal.displayInfo('paste over')

    def copyActionFn(self):
        if self.copyDict and self.bsList.selectedItems():
            self.selID = []
            numID = []
            for bsi in self.bsList.selectedItems():
                self.selID.append(str(bsi.text()))
                numID.append(int(bsi.toolTip()))
            self.selID.sort()

            self.copyDict = {}
            tempMeshs = []

            oTypeKey = 'relative'
            typeSel = self.radGrp.checkedId()
            if typeSel == 1:
                oTypeKey = 'absolute'

            for nid in numID:
                shapeMesh = transClass.extrudeBlendshapeTarget(self.blendshapeNode, nid)

                tempMeshs.append(shapeMesh['meshName'])

                dataDict = transClass.getMuteList(self.theFrom, shapeMesh['meshName'], oType=oTypeKey)

                self.copyDict.update({shapeMesh['bsAttr']: dataDict})
            # print tempMeshs
            cmds.delete(tempMeshs)

        om.MGlobal.displayInfo('copy over')

    def refreshNodeBs(self):
        if self.theFrom:
            self.bsList.clear()
            selShape = cmds.listRelatives(self.theFrom, s=True)
            lsConnBs = cmds.listConnections(selShape[0] + '.inMesh', s=True)
            if lsConnBs:




                self.blendshapeNode = lsConnBs[0]
                bsCrtvs = cmds.listAttr(self.blendshapeNode + '.weight', m=1)
                for count, b in enumerate(bsCrtvs):
                    itm = qw.QListWidgetItem()
                    itm.setText(b)
                    itm.setToolTip(str(count))
                    self.bsList.addItem(itm)



    def refreshTransBs(self):
        if self.theFrom:
            
            self.bsListp3.clear()
            
            selShape = cmds.listRelatives(self.theFrom, s=True)
            
            lsConnBs = cmds.listConnections(selShape[0] + '.inMesh', s=True)
            
            if lsConnBs:
                
                tempMeshs = []

                self.blendshapeNode = lsConnBs[0]
                bsCrtvs = cmds.listAttr(self.blendshapeNode + '.weight', m=1)
                for count, b in enumerate(bsCrtvs):
                    itm = qw.QListWidgetItem()
                    itm.setText(b)
                    itm.setToolTip(str(count))
                    self.bsListp3.addItem(itm)

                    #shapeMesh = transClass.extrudeBlendshapeTarget(self.blendshapeNode, count)

                    #tempMeshs.append(shapeMesh['meshName'])

                #self.tempFrom = cmds.duplicate(self.theFrom)[0]
                #tempBs = cmds.blendShape(tempMeshs,self.tempFrom)
                    #dataDict = transClass.getMuteList(self.theFrom, shapeMesh['meshName'],oType=oTypeKey)

                    #self.copyDict.update({shapeMesh['bsAttr']: dataDict})
                #print tempMeshs
                #cmds.delete(tempMeshs)

    def mirrorActionFn(self):
        if self.theTo and self.theFrom:
            sender = self.sender()
            keyStr = sender.toolTip()

            pm.undoInfo(openChunk=True)

            if keyStr == 'ltor':


                for s in self.theTo:
                    #try:
                    fgMirrorClass.symmetryFunciton(s, fgMirrorClass.fwdMirrorDict, fgMirrorClass.midVerts)
                    #except:
                     #   pass

            elif keyStr == 'rtol':
                for s in self.theTo:
                    #try:
                    fgMirrorClass.symmetryFunciton(s, fgMirrorClass.negMirrorDict, fgMirrorClass.midVerts)
                    #except:
                    #    pass
            elif keyStr == 'mirror':
                for s in self.theTo:
                    #try:
                    fgMirrorClass.mirrorFunction(s, fgMirrorClass.fwdMirrorDict)
                    #except:
                     #   pass
            pm.undoInfo(closeChunk=True)

    def pickItem(self):
        seltemp = cmds.ls(sl=1)
        sel = []
        for s in seltemp:
            if cmds.objectType(s)=='transform':
                sel.append(s)

        if sel:
            sender = self.sender()
            if sender.toolTip() == 'orig':
                self.txtOrig.setText(sel[0])
                self.mirrorAxis = 0
                self.theFrom = sel[0]
                if self.nowTab == 0:
                    sender.setStyleSheet(Mstyle.QPushButton())
                    fgMirrorClass.loadSourceModel(str(sel[0]))
                    om.MGlobal.displayInfo('load orig at full function')
                else:
                    sender.setStyleSheet(Mstyle.QPushButton(kw='b'))
                    om.MGlobal.displayInfo('load orig at simple function')
            if sender.toolTip() == 'targ':
                cbStr = ''
                if self.nowTab == 2:
                    cbStr = sel[0]
                    sender.setStyleSheet(Mstyle.QPushButton(kw='b'))
                else:
                    cbStr = ','.join(sel)
                    sender.setStyleSheet(Mstyle.QPushButton())
                self.txtTarg.setText(cbStr)
                self.theTo = copy.deepcopy(sel)






    def testFn(self):
        print self.radGrp.checkedId()

    def initData(self):
        self.dataSettings = QtCore.QSettings("customToolWindowSettings", "customToolWindowSettings")

    def refreshCallCount(self):
        if self.HOLY_ID == 0:
            return

        try:
            import HolyCmdLog
            HolyCmdLog.saveusercmdlogtofile(self.HOLY_ID)
        except:pass

    def readSettings(self):
        self.dataSettings.beginGroup('customToolWindowSettings_mainWindow')
        windowGeometry = self.dataSettings.value('window_geometry')
        windowState = self.dataSettings.value('window_state')
        self.restoreGeometry(windowGeometry)
        self.restoreState(windowState)
        self.dataSettings.endGroup()

    def writeSettings(self):
        self.dataSettings.beginGroup('customToolWindowSettings_mainWindow')
        self.dataSettings.setValue('window_geometry', self.saveGeometry())
        self.dataSettings.setValue('window_state', self.saveState())
        self.dataSettings.endGroup()

    def resizeEvent(self,e):
        realTimeWidth = self.width()
        #self.txtOrig.setMinimumWidth(realTimeWidth * 0.6)
        #self.txtOrig.setFixedSize(realTimeWidth*0.6, 18)
        #self.txtTarg.setFixedSize(realTimeWidth*0.6, 18)

    def closeEvent(self,event):
        self.writeSettings()

def main():
    '''
    holy polygon reduce faces editor
    '''
    app = qw.QApplication.instance()
    if not app:
        app = qw.QApplication([])

    for widget in app.topLevelWidgets():
        #try:
        if widget.objectName() == transShapeUIClass.WINDOW_OBJECT_NAME:
            widget.close()
            widget.deleteLater()
        #except:
        #   pass

    window = transShapeUIClass(parent=MayaParent)
    window.show()

    window.raise_()
    try:
        sys.exit(app.exec_())
    except: pass

if __name__ == '__main__':
    main()
    #window = splitBlendshapeUIClass(parent=MayaParent)
    #window.show()