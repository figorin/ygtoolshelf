import maya.OpenMaya as om
import maya.cmds as cmds
import re
import math




class mirrorVert_Class():

    def __init__(self):
        self.fwdMirrorDict = {}

        self.negMirrorDict = {}

        self.combieMirrorDict = {}

        self.midVerts = []
        # storage forward mirror dict and negtive mirror dict.

    def selVertByIntArray(self,polyName,inputArray,add=False):
        if not add:
            cmds.select(cl=1)
        for i in inputArray:
            vertString = '%s.vtx[%s]' % (polyName,str(i))
            cmds.select(vertString,add=1)

    def selEdgeByIntArray(self,polyName,inputArray,add=False):
        if not add:
            cmds.select(cl=1)
        for i in inputArray:
            vertString = '%s.e[%s]' % (polyName,str(i))
            cmds.select(vertString,add=1)

    def getVertsPosDict(self, meshName, space='object'):
        if not isinstance(meshName, str):
            print 'input type error,please input string name only.'
            return False
        # 1 # initialize a selectionList holder
        selectionLs = om.MSelectionList()
        selectionLs.add(meshName)
        # 2 # get the selected object in the viewport and put it in the selection list
        #om.MGlobal.getActiveSelectionList(selectionLs)

        # 3 # initialize a dagpath object
        dagPath = om.MDagPath()

        # 4 # populate the dag path object with the first object in the selection list
        selectionLs.getDagPath(0, dagPath)

        # ___________Query vertex position ___________

        if space == 'object':
            theSpace = om.MSpace.kObject
        elif space == 'world':
            theSpace = om.MSpace.kWorld
        else:
            print 'space key word error.plse choose "world" or "object"'
            return False


        mfnObject = om.MFnMesh(dagPath)

        vertCount = mfnObject.numVertices()
        pts = om.MPoint()

        baseDict = {}
        for i in range(vertCount):
            mfnObject.getPoint(i, pts, theSpace)

            baseDict.update({i: [pts.x, pts.y, pts.z]})
        return baseDict


    def interceptFloat_posDict(self,posDict, length=4):
        spData = {}
        for key, val in posDict.items():
            tempv = []
            for v in val:
                sv = round(v, length)
                tempv.append(sv)

            spData.update({key: tempv})
        return spData

    def quickSort_array(self, myList, start, end, kw=1):
        if start < end:
            i, j = start, end
            base = myList[i]

            while i < j:
                while (i < j) and (myList[j][kw] >= base[kw]):
                    j = j - 1

                myList[i] = myList[j]

                while (i < j) and (myList[i][kw] <= base[kw]):
                    i = i + 1
                myList[j] = myList[i]
            myList[i] = base

            self.quickSort_array(myList, start, i - 1)
            self.quickSort_array(myList, j + 1, end)
        return myList

    def approximateArray(self, array1, array2, threshold=0.0005):
        if len(array1) == len(array2):
            checkArray = []
            for count in range(len(array1)):
                val = array1[count]
                if (val + threshold) > array2[count] > (val - threshold):
                    checkArray.append(True)
                else:
                    checkArray.append(False)
            if False in checkArray:
                return False
            else:
                return True
        else:
            return False

    def generateMirrorDict(self, inputdataFormat,splitAxis = 2,patternDirection = 1, shirnkRange = 0.1):

        # patternDirection: if pattern direction
        # ----> 1: means start from forward to negtive.
        # ----> 0: means start fromt negtive to forward.

        spData = self.interceptFloat_posDict(inputdataFormat)
        midArray = []
        fwdSide = {}
        negSide = {}

        fwdSideInv = {}
        negSideInv = {}
        for key, val in spData.items():
            if val[splitAxis] > 0:
                fwdSide.update({key: val})
                fwdSideInv.update({str(val): key})

            elif val[splitAxis] < 0:
                negSide.update({key: val})
                negSideInv.update({str(val): key})

            else:
                #print midArray
                midArray.append(key)


        searchKey = []
        patternKey = []

        searchDict = {}
        patternDict = {}

        if patternDirection:
            for pk in fwdSideInv.keys():
                lst = eval(pk)
                searchKey.append(lst)

            for pk in negSideInv.keys():
                lst = eval(pk)
                patternKey.append(lst)

            searchDict = fwdSideInv
            patternDict = negSideInv
        else:

            for pk in fwdSideInv.keys():
                lst = eval(pk)
                patternKey.append(lst)

            for pk in negSideInv.keys():
                lst = eval(pk)
                searchKey.append(lst)

            searchDict = negSideInv
            patternDict = fwdSideInv

        maxCount = len(searchKey)
        if len(searchKey) != len(patternKey):
            print 'verts count not match. do you want to select not match points?'
            # if yes. go to breach functions.
            # if no . retrn false

        searchKey  = self.quickSort_array(searchKey, 0, maxCount-1)
        patternKey = self.quickSort_array(patternKey, 0, maxCount-1)


        returnDict = {}
        rangeVal = int(maxCount * shirnkRange)
        for count in range(maxCount):

            minRange = count - rangeVal
            if minRange < 0:
                minRange = 0

            maxRange = count + rangeVal
            if maxRange > maxCount:
                maxRange = maxCount

            matchArray = []
            for pCount in range(minRange, maxRange):
                tempSk = list(searchKey[count])
                tempSk[splitAxis] *= -1

                ans = self.approximateArray(tempSk, patternKey[pCount])
                if ans:
                    sVert = searchDict[str(searchKey[count])]
                    pVert = patternDict[str(patternKey[pCount])]
                    matchArray.append({sVert: pVert})

            if len(matchArray) != 1:
                pass

            else:
                returnDict.update(matchArray[0])

        return [returnDict, midArray]

    def loadSourceModel(self, modelName, spAxis = 0):

        dataFormat = self.getVertsPosDict(modelName)

        dataTemp = self.generateMirrorDict(dataFormat,splitAxis = spAxis)

        self.midVerts = dataTemp[1]

        self.fwdMirrorDict = dataTemp[0]

        self.negMirrorDict = {v: k for k, v in self.fwdMirrorDict.items()}

        self.combieMirrorDict = dict(self.fwdMirrorDict.items() + self.negMirrorDict.items())

        # don's run on polygon with 40 thousand faces.really lag.spend 133 seconds

    #mvc = mirrorVert_Class()

    def markVertColor(self, meshName, theArray, dColor=[0.8,0.2,0.2]):
        # now work,need fix.
        # no bug or error warning.just not work.
        activeList = om.MSelectionList()
        activeList.add(meshName)
        omDag = om.MDagPath()
        activeList.getDagPath(0, omDag)
        omDag.extendToShape()


        omMesh = om.MFnMesh(omDag)

        vertColorList = om.MColorArray()
        omMesh.getVertexColors(vertColorList)

        lenVertexList = vertColorList.length()

        fnComponent = om.MFnSingleIndexedComponent()
        fullComponent = fnComponent.create(om.MFn.kMeshVertComponent)

        fnComponent.setCompleteData(lenVertexList)


        vertIndexList = om.MIntArray()
        fnComponent.getElements(vertIndexList)

        for lvl in range((lenVertexList)):
            #print theArray[i]
            #print ''
            #print lvl
            vertColorList[lvl].r = dColor[0]
            vertColorList[lvl].g = dColor[1]
            vertColorList[lvl].b = dColor[2]
            vertIndexList.append(theArray[lvl])

        # print vertIndexList

        omMesh.setVertexColors(vertColorList, vertIndexList, None)
        print 'gai se wan cheng'
        return True

    def symmetryFunciton(self, targetModel, symmetryDict={}, middleArray=[],mirrorAxis=0,py=True):
        if py:
            for key, val in symmetryDict.items():
                frPt = '%s.vtx[%i]' % (targetModel,symmetryDict[key])
                getPos = cmds.xform(frPt,q=1,os=1,t=1)
                toPt = '%s.vtx[%i]' % (targetModel,symmetryDict[val])
                toPt[0] *= -1
                cmds.xform(toPt,os=1,t=1)
        else:
            activeList = om.MSelectionList()
            activeList.add(targetModel)
            omDag = om.MDagPath()
            activeList.getDagPath(0, omDag)

            omMesh = om.MFnMesh(omDag)

            # pass shape part
            for key, val in symmetryDict.items():
                omPoint = om.MPoint()
                omMesh.getPoint(int(key), omPoint, om.MSpace.kWorld)

                ta = [omPoint.x, omPoint.y, omPoint.z, omPoint.w]
                ta[mirrorAxis] *= -1
                negPoint = om.MPoint(float(ta[0]), float(ta[1]), float(ta[2]), float(ta[3]))

                omMesh.setPoint(int(val), negPoint, om.MSpace.kObject)
            if middleArray:
                for i in middleArray:
                    omPoint = om.MPoint()
                    omMesh.getPoint(int(i), omPoint, om.MSpace.kWorld)
                    ta = [omPoint.x, omPoint.y, omPoint.z, omPoint.w]
                    ta[mirrorAxis] *= 0
                    negPoint = om.MPoint(float(ta[0]), float(ta[1]), float(ta[2]), float(ta[3]))

                    omMesh.setPoint(int(i), negPoint, om.MSpace.kObject)

        return True

    def mirrorFunction(self, targetModel, symmetryDict={}, mirrorAxis=0):
        #self.fwdMirrorDict
        activeList = om.MSelectionList()
        activeList.add(targetModel)
        omDag = om.MDagPath()
        activeList.getDagPath(0, omDag)

        omMesh = om.MFnMesh(omDag)

        ptArray = om.MPointArray()
        omMesh.getPoints(ptArray, om.MSpace.kObject)
        ptArrayNeg = om.MPointArray()
        for i in range(int(ptArray.length() * 1.0)):
            tempList = [ptArray[i].x, ptArray[i].y, ptArray[i].z, ptArray[i].w]
            tempList[mirrorAxis] *= -1
            ptArrayNeg.append(tempList[0], tempList[1], tempList[2], tempList[3])

        for key, val in symmetryDict.items():
            tempKey = [ptArrayNeg[int(key)].x,
                       ptArrayNeg[int(key)].y,
                       ptArrayNeg[int(key)].z,
                       ptArrayNeg[int(key)].w]
            tempVal = [ptArrayNeg[int(val)].x,
                       ptArrayNeg[int(val)].y,
                       ptArrayNeg[int(val)].z,
                       ptArrayNeg[int(val)].w]
            ptArrayNeg.set(int(key), tempVal[0], tempVal[1], tempVal[2], tempVal[3])
            ptArrayNeg.set(int(val), tempKey[0], tempKey[1], tempKey[2], tempKey[3])

        omMesh.setPoints(ptArrayNeg, om.MSpace.kObject)

        return True


if '__main__' == __name__:
    print 'enter'

    '''
    import maya.OpenMaya as om
    import maya.cmds as cmds

    vtxName = 'pSphere1'
    mirrorAxis = 0

    #gg = mirrorVert_Class()
    #gg.loadSourceModel(vtxName, spAxis=mirrorAxis)


    drawMorColor = False
    if drawMorColor:
        gg.markVertColor(vtxName, gg.midVerts, dColor=[0.0, 0.0, 0.0])
        gg.markVertColor(vtxName, gg.fwdMirrorDict.keys(), dColor=[0.8, 0.2, 0.2])
        gg.markVertColor(vtxName, gg.negMirrorDict.keys(), dColor=[0.2, 0.2, 0.8])



    changeName = 'pSphere2'
    gg.mirrorFunction(changeName, gg.fwdMirrorDict)
    # need a undo function.

    '''
