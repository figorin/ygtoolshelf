import json
import os


class jsonBaseClass():

    def unitizeSlantLine(self,inputPath):
        outStr = str(inputPath)
        outStr = outStr.replace('\\\\','\\')
        outStr = outStr.replace('/','\\')
        return outStr


    def saveJson(self,thePath,infomations):
        thePath = self.unitizeSlantLine(thePath)

        dumpData = json.dumps(infomations)

        with open(thePath,'w') as f:
            f.write(dumpData)


    def loadJson(self, thePath, codec = 'utf-8'):
        thePath = self.unitizeSlantLine(thePath)
        if os.path.isdir(thePath):
            print "it's a directory"
            return False

        elif os.path.isfile(thePath):
            with open(thePath,'r') as f:
                r = f.read()
                r=r.decode(codec)
                theVal = json.loads(r)

            return theVal

def main():
    temp = jsonBaseClass()
    return  temp