'''
@Date: 2020-04-25 22:29:07
LastEditors: figo-ds
LastEditTime: 2020-12-24 16:16:39
FilePath: \undefinedc:\gitlab\sstoolshelf\debug\FgToolshelf\depository\Rig\openGetDialog_rig.py
'''
import sys
import os
try:
    toolPath = os.environ['FGPIPEROOT']
except KeyError:
    # toolPath = r'S:\PipeProgram\figoPipe\pipelineMainProgram'
    toolPath = r'C:\gitLab\brownser01'
finally:
    if toolPath not in sys.path:
        sys.path.append(toolPath)

import figoBrownser.platforms.maya.rig.browser_Rig_Get as rig_get
reload(rig_get)

global FGbrownserWindow
FGbrownserWindow = None
FGbrownserWindow = rig_get.main()