# weights

#### 介绍
这是一个自动计算关节、次级、脊椎、裙子等部分权重、并通过bezier 曲线进行精细控制的Maya权重计算工具。 


#### 使用视频
[https://www.bilibili.com/video/av92491279/](https://www.bilibili.com/video/av92491279/)

#### BUG反馈
[https://afdian.net/group/e268edc45df411eaa59b52540025c377](https://afdian.net/group/e268edc45df411eaa59b52540025c377)

#### C++极速版
[https://afdian.net/p/a29f0368518811ea828a52540025c377](https://afdian.net/p/a29f0368518811ea828a52540025c377)

#### 开发文档
[http://lush_ma.gitee.io/weights](http://lush_ma.gitee.io/weights)

#### 开发教程
[https://www.aboutcg.org/courseDetails/808/introduce](https://www.aboutcg.org/courseDetails/808/introduce)



