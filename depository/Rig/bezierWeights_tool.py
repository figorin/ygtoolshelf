#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
Author: figo
Date: 2020-09-16 16:14:53
LastEditTime: 2020-12-15 16:52:55
LastEditors: figo - uz
Description: 
FilePath: \gitlab\sstoolshelf\debug\FgToolshelf\depository\Rig\bezierWeights_tool.py
copyright: figo software 2020-2021
'''
from Rig.bezierWeights import ui as ui

global mengyaBezierUI

try:
    mengyaBezierUI = ui.BezierWeight()
finally:
    mengyaBezierUI.show()

