import maya.cmds as cmds
dt_cell = {'con_attr':'IndexFinger2_R.rotateY','con_val':[0,-90],'dn_attr':'blendShapeIndex_mod.right','dn_val':[0,1]}
data = [
    {'con_attr':'IndexFinger1_L.rotateY','con_val':[0,90],'dn_attr':'hand_bs.index0_L','dn_val':[0,1]},
    {'con_attr':'IndexFinger1_R.rotateY','con_val':[0,90],'dn_attr':'hand_bs.index0_R','dn_val':[0,1]},
    
    {'con_attr':'IndexFinger2_L.rotateY','con_val':[0,90],'dn_attr':'hand_bs.index1_L','dn_val':[0,1]},
    {'con_attr':'IndexFinger2_R.rotateY','con_val':[0,90],'dn_attr':'hand_bs.index1_R','dn_val':[0,1]},
    
    {'con_attr':'IndexFinger3_L.rotateY','con_val':[0,90],'dn_attr':'hand_bs.index2_L','dn_val':[0,1]},
    {'con_attr':'IndexFinger3_R.rotateY','con_val':[0,90],'dn_attr':'hand_bs.index2_R','dn_val':[0,1]},
    

    {'con_attr':'MiddleFinger1_L.rotateY','con_val':[0,90],'dn_attr':'hand_bs.middle0_L','dn_val':[0,1]},
    {'con_attr':'MiddleFinger1_R.rotateY','con_val':[0,90],'dn_attr':'hand_bs.middle0_R','dn_val':[0,1]},
    
    {'con_attr':'MiddleFinger2_L.rotateY','con_val':[0,90],'dn_attr':'hand_bs.middle1_L','dn_val':[0,1]},
    {'con_attr':'MiddleFinger2_R.rotateY','con_val':[0,90],'dn_attr':'hand_bs.middle1_R','dn_val':[0,1]},
    
    {'con_attr':'MiddleFinger3_L.rotateY','con_val':[0,90],'dn_attr':'hand_bs.middle2_L','dn_val':[0,1]},
    {'con_attr':'MiddleFinger3_R.rotateY','con_val':[0,90],'dn_attr':'hand_bs.middle2_R','dn_val':[0,1]},
    

    {'con_attr':'RingFinger1_L.rotateY','con_val':[0,90],'dn_attr':'hand_bs.ring0_L','dn_val':[0,1]},
    {'con_attr':'RingFinger1_R.rotateY','con_val':[0,90],'dn_attr':'hand_bs.ring0_R','dn_val':[0,1]},
    
    {'con_attr':'RingFinger2_L.rotateY','con_val':[0,90],'dn_attr':'hand_bs.ring1_L','dn_val':[0,1]},
    {'con_attr':'RingFinger2_R.rotateY','con_val':[0,90],'dn_attr':'hand_bs.ring1_R','dn_val':[0,1]},
    
    {'con_attr':'RingFinger3_L.rotateY','con_val':[0,90],'dn_attr':'hand_bs.ring2_L','dn_val':[0,1]},
    {'con_attr':'RingFinger3_R.rotateY','con_val':[0,90],'dn_attr':'hand_bs.ring2_R','dn_val':[0,1]},
    


    {'con_attr':'PinkyFinger1_L.rotateY','con_val':[0,90],'dn_attr':'hand_bs.pinky0_L','dn_val':[0,1]},
    {'con_attr':'PinkyFinger1_R.rotateY','con_val':[0,90],'dn_attr':'hand_bs.pinky0_R','dn_val':[0,1]},
    

    {'con_attr':'PinkyFinger2_L.rotateY','con_val':[0,90],'dn_attr':'hand_bs.pinky1_L','dn_val':[0,1]},
    {'con_attr':'PinkyFinger2_R.rotateY','con_val':[0,90],'dn_attr':'hand_bs.pinky1_R','dn_val':[0,1]},
    

    {'con_attr':'PinkyFinger3_L.rotateY','con_val':[0,90],'dn_attr':'hand_bs.pinky2_L','dn_val':[0,1]},
    {'con_attr':'PinkyFinger3_R.rotateY','con_val':[0,90],'dn_attr':'hand_bs.pinky2_R','dn_val':[0,1]},
    


    {'con_attr':'ThumbFinger3_L.rotateY','con_val':[0,90],'dn_attr':'hand_bs.thumb2_L','dn_val':[0,1]},
    {'con_attr':'ThumbFinger3_R.rotateY','con_val':[0,90],'dn_attr':'hand_bs.thumb2_R','dn_val':[0,1]},
    

    {'con_attr':'ThumbFinger2_L.rotateY','con_val':[0,90],'dn_attr':'hand_bs.thumb1_L','dn_val':[0,1]},
    {'con_attr':'ThumbFinger2_R.rotateY','con_val':[0,90],'dn_attr':'hand_bs.thumb1_R','dn_val':[0,1]},
    
    {'con_attr':'Elbow_L.rotateZ','con_val':[0,90],'dn_attr':'arm_bs.left','dn_val':[0,1]},
    {'con_attr':'Elbow_R.rotateZ','con_val':[0,90],'dn_attr':'arm_bs.right','dn_val':[0,1]},
    
    
    {'con_attr':'Elbow_L.rotateZ','con_val':[0,90],'dn_attr':'upcloth_bs.left','dn_val':[0,1]},
    {'con_attr':'Elbow_R.rotateZ','con_val':[0,90],'dn_attr':'upcloth_bs.right','dn_val':[0,1]},
    
    # {'con_attr':'Ankle_L.rotateZ','con_val':[0,50],'dn_attr':'body_main_mod_00BS.ankle_left','dn_val':[0,1]},
    # {'con_attr':'Ankle_R.rotateZ','con_val':[0,50],'dn_attr':'body_main_mod_00BS.ankle_right','dn_val':[0,1]},
    
    {'con_attr':'Knee_L.rotateZ','con_val':[0,-90],'dn_attr':'body_bs.left','dn_val':[0,1]},
    {'con_attr':'Knee_R.rotateZ','con_val':[0,-90],'dn_attr':'body_bs.right','dn_val':[0,1]},

    {'con_attr':'Knee_L.rotateZ','con_val':[0,-90],'dn_attr':'kuzi_bs.left','dn_val':[0,1]},
    {'con_attr':'Knee_R.rotateZ','con_val':[0,-90],'dn_attr':'kuzi_bs.right','dn_val':[0,1]}



]

for dt in data:
    cmds.setAttr(dt['con_attr'], dt['con_val'][0])
    cmds.setDrivenKeyframe(dt['dn_attr'], cd=dt['con_attr'], v=dt['dn_val'][0])
    cmds.setAttr(dt['con_attr'], dt['con_val'][1])
    cmds.setDrivenKeyframe(dt['dn_attr'], cd=dt['con_attr'], v=dt['dn_val'][1])
    
    cmds.setAttr(dt['con_attr'], dt['con_val'][0])

    