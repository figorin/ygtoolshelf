import maya.cmds as cmds

def findItem(selObj=None): #---------------------- Main Def ------------------------------
    sceneObjList = cmds.ls()                 
    sameNameList = []                        
    targetNameList = []                      
    for i in sceneObjList:                   
        if len(i.split('|')) > 1:            
            sameNameList.append(i)
    
    if selObj != None:
        for target in selObj:
            getOlny = target.split('|')
            onlyName = getOlny[len(getOlny)-1]
            targetNameList.append(onlyName)
        
        currentList = []
        targetNameList = list(set(targetNameList))
        for targetName in targetNameList:
            for sameName in sameNameList:
                getOnly = sameName.split('|')
                onlyName = getOnly[len(getOnly)-1]
                if onlyName == targetName:
                    currentList.append(sameName)
        return currentList
    else: 
        return sameNameList#-------------------------------- End Main Def --------------------------------
fi = findItem()
for count,i in enumerate(fi):
    if cmds.objExists(i):
        if cmds.objectType(i)=='transform':
            newName = i.split('|')[-1]
            newName = newName+str(count)
            cmds.rename(i,newName)
for i in range(20):
    print 'loop check ' + str(i)
    fi = findItem()
    if fi:
        for count,i in enumerate(fi):
            if cmds.objExists(i):
                newName = i.split('|')[-1]
                newName = newName+str(count)
                cmds.rename(i,newName)
    else:
        break