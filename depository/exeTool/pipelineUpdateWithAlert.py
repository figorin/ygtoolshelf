#!/usr/bin/env python
# coding=utf-8
'''
@Author: figo-home
@Date: 2020-06-15 10:32:17
@LastEditTime: 2020-07-17 18:57:20
@LastEditors: figo
@Description: 
@FilePath: \gitLab\sstoolshelf\publish\FgToolshelf\depository\exeTool\pipelineUpdateWithAlert.py
@copyright: figo software 2020-2021
'''
# -*- coding: utf-8 -*-
__author__ = 'figo Lin'

# from figoBrownser.bin.Qt import QtWidgets as qw, QtGui, QtCore, IsPySide, IsPySide2
import logging
import maya.cmds as cmds
import os
import json
import copy
import time
import hmac
import hashlib
import base64
import urllib
import random

import requests


import getpass
class MengMeng_alertToDingDing():

    def proc(self):
        
        os.system(r"C:\gitLab\brownser01\publishToYoozoo_auto.bat")
        
        self.webhook = 'https://oapi.dingtalk.com/robot/send?access_token=c00b9a7653f965c7e8b8d869f87453b933f0c68b22149e195e7aad0ea06c2889'
        self.secret = 'SEC6c73955c706706a001c6659af601a4bde4b01363d7471c536d166f8ad1bce36e'
        self.accessToken = 'c00b9a7653f965c7e8b8d869f87453b933f0c68b22149e195e7aad0ea06c2889'
        will = [u'路是脚踏出来的，历史是人写出来的，人的每一步行动都在书写自己的历史',
                u'头脑是狭小的，而他却隐藏着思想，眼睛只是一个小点，他却能环视辽阔的天地。',
                u'在一个崇高的目的支持下，不停地工作，即使慢、也一定会获得成功。',
                u'一切事无法追求完美，唯有追求尽力而为。这样心无压力，出来的结果反而会更好。'
                u'富贵不淫贫贱乐，男儿到此是豪雄。',
                u'质朴却比巧妙的言辞更能打动我的心。',
                u'无论做什么事情，只要肯努力奋斗，是没有不成功的。',
                u'手莫伸，伸手必被捉。党与人民在监督，万目睽睽难逃脱。汝言惧捉手不伸，他道不伸能自觉，其实想伸不敢伸，人民咫尺手自缩',
                u'自以为聪明的人往往是没有好下场的。世界上最聪明的人是最老实的人，因为只有老实人才能经得起事实和历史的考验',
                u'每个人身上都有一副小翅膀，有的人在头上，有的人在手上，有的人在腿上，有的人在心上……翅膀在哪儿，你的才华就在哪儿',
                u'快乐活在当下，尽心就是完美。',
                u'人的生命是有限的，可是，为人民服务是无限的，我要把有限的生命，投入到无限为人民服务之中去。'
                u'成功与失败的分水岭，可以用这五个字来表达——我没有时间。',
                u'我只担心一件事，我怕我配不上自己所受的苦难。',
                u'如果一个人不知道他要驶向哪个码头，那么任何风都不会是顺风。',
                
        
        
        ]
        
        #构建请求头部
        header = {
                "Content-Type": "application/json",
                "Charset": "UTF-8"
        }
        #构建签名
        signData = self.makesign()
        #构建请求数据
        # pattern = u'%(user)s 发布了 %(dep)s \n[ %(cata)s ] %(name)s - %(varient)s'
        #\n     当前第%(version)s版\n     时间：%(timestr)s\n     备注：%(comment)s'
        dataDict = {}
        dataDict['user'] = getpass.getuser()
        dataDict['timestr'] = time.strftime("%Y-%m-%d %H:%M:%S", time.localtime(time.time())) 

        dataDict['comment'] = u'产线更新了，远程的小伙伴拉一下新的脚本'
        # theId = self.getMedia_id(previewpath)
        # tex = pattern % dataDict
        mdmsg = u'# %(user)s 更新了产线工具  \n  *%(timestr)s*  \n  %(comment)s  ' % dataDict
        message ={

            "msgtype": "markdown",
            "markdown":{
                "title": "%(user)s 更新了产线工具" % dataDict,
                "text": mdmsg
            },
            "at": {

                "isAtAll": True
            }

        }
        #对请求的数据进行json封装
        message_json = json.dumps(message)
        #发送请求
        signData['webhook'] = self.webhook
        postUrl = '%(webhook)s&timestamp=%(timestamp)s&sign=%(sign)s'  % signData
        info = requests.post(url=postUrl,data=message_json,headers=header)
        #打印返回的结果
        print(info.text)
        print '*'*100


    def makesign(self):
        timestamp = long(round(time.time() * 1000))
        secret = self.secret
        secret_enc = bytes(secret).encode('utf-8')
        string_to_sign = '{}\n{}'.format(timestamp, secret)
        string_to_sign_enc = bytes(string_to_sign).encode('utf-8')
        hmac_code = hmac.new(secret_enc, string_to_sign_enc, digestmod=hashlib.sha256).digest()
        sign = urllib.quote_plus(base64.b64encode(hmac_code))
        return {'timestamp':timestamp,'sign':sign}
        # print(timestamp)
        # print(sign)
node = MengMeng_alertToDingDing()
node.proc()
if __name__ == "__main__":
    node = MengMeng_alertToDingDing()
    node.proc()
    #node = spoilerItem.FrameDialog()
    # node.title="File Struct"
    #node.widget = qw.QPushButton('yes')
    #node.widget.setFixedHeight(300)
    