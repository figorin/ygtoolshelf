# -*- coding: utf-8 -*-
import os
import sys
import subprocess, os

def getRootPath():
    loadPath = ''
    for path in sys.path:
        print path+'\\pathfile.browser'
        if os.path.exists(path+'\\pathfile.browser'):
            loadPath = path

    return loadPath

def main(rccPath = 'C:/Python27/Lib/site-packages/PyQt4',rootPath=''):
    if not rootPath:
        rootPath = getRootPath()
    rootPath = rootPath.replace('\\','/')

    if os.path.exists(rootPath + '/src/resourcesAA.py'):
        os.remove(rootPath + '/src/resourcesAA.py')


    images = os.listdir(rootPath + '/src/icon')
    writter = os.listdir(rootPath + '/src/writer_icon')
    qss = os.listdir(rootPath + '/src/qss')
    with open(rootPath + '/src/resourcesAA.qrc', 'w+') as f:
        f.write(u'<!DOCTYPE RCC>\n<RCC version="1.0">\n<qresource>\n')

        for item in images:
            f.write(u'<file alias="icon/'+ item +'">icon/'+ item +'</file>\n')

        for item in qss:
            f.write(u'<file alias="qss/'+ item +'">qss/'+ item +'</file>\n')

        for item in writter:
            f.write(u'<file alias="writer_icon/'+ item +'">writer_icon/'+ item +'</file>\n')

        f.write(u'</qresource>\n</RCC>')
    spellCommand = rccPath + '/pyrcc4.exe -o %s/src/resourcesAA.py %s/src/resourcesAA.qrc' % (rootPath,rootPath)
    pipe = subprocess.Popen(spellCommand, stdout = subprocess.PIPE, stdin = subprocess.PIPE, stderr = subprocess.PIPE, creationflags=0x08)

    #pipe = os.system()
if __name__ == '__main__':
    testPath = r'C:\gitLab\ssToolShelf'
    if testPath not in sys.path:
        sys.path.append(testPath)
    # maya本身不带rcc，需要自己装一个pyqt或者pyside，把main的rccpath指到那个python里面的目录里
    main(rootPath=r'C:\gitLab\ssToolShelf')