import os
import sys
import maya.cmds as cmds
#from maya.OpenMayaUI import MQtUtil


def getShelfEnvironment():
    isFound = False
    for i in sys.path:
        thePath = i + '/installPath_ssToolShelf.json'
        if os.path.exists(thePath):
            if '\\' in thePath:
                thePath = thePath.replace('\\','/')
            spPath = thePath.split('/')

            isFound = '/'.join(spPath[:-2])

            os.environ["ssToolShelfPath"] = '/'.join(spPath[:-3])
            break
            
    return isFound

def openTestPort():
    if not cmds.commandPort(':17092', q=True):
        cmds.commandPort(n=':17092',sourceType="python")

def initShelfPath():
    print 'initShelfPath'
    shelfPath = getShelfEnvironment()
    _sitepackage = shelfPath + '/site-packages'
    if _sitepackage not in sys.path:
        sys.path.append(_sitepackage)
        print '_sitepackage',_sitepackage


    _pathDict = {
        'MAYA_PLUG_IN_PATH':shelfPath + '/plug-ins',
        'MAYA_SCRIPT_PATH':shelfPath + '/scripts',
        'XBMLANGPATH':shelfPath + '/icons',
        'MAYA_MODULE_PATH':shelfPath + '/modules'
    }

    for key,val in _pathDict.items():
        #_pluginPath = self.shelfPath + '/plug-ins'
        spPlugin = os.environ[key].split(';')
        if val not in os.environ[key]:
            spPlugin.append(val)
            os.environ[key] = ';'.join(spPlugin)


#MAYA_PLUGIN_PATH  MAYA_PLUG_IN_PATH

#MAYA_SCRIPT_PATH

#MAYA_MODULE_PATH

#XBMLANGPATH
def initSporePath():
    shelfPath = getShelfEnvironment()
    sys.path.append(shelfPath + '/plug-ins/spore-master/scripts')
    sys.path.append(shelfPath + '/plug-ins/spore-master/scripts/utils')
    sys.path.append(shelfPath + '/plug-ins/spore-master/scripts/ui')
    sys.path.append(shelfPath + '/plug-ins/spore-master/scripts/AETemplate')
    sys.path.append(shelfPath + '/plug-ins/spore-master/scripts/data')

initShelfPath()
print 'in ssToolShelf'
#cmds.loadPlugin( 'spore.py' )