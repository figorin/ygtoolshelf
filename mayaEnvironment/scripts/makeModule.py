#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
Author: figo
Date: 2020-09-16 16:14:57
LastEditTime: 2020-12-14 18:31:07
LastEditors: figo - uz
Description: 
FilePath: \gitlab\sstoolshelf\debug\FgToolshelf\mayaEnvironment\scripts\makeModule.py
copyright: figo software 2020-2021
'''
import os
import json
import sys
import maya.cmds as cmds

import logging
logging.basicConfig(level = logging.INFO,format = '%(levelname)s|%(asctime)s - %(name)s - %(funcName)s : %(message)s')
logger = logging.getLogger('sstoolshelf - makeModule')
logger.setLevel(logging.DEBUG)

class initSSToolShelf():

    def makeFile(self):
        inputPath = os.environ["ssToolShelfPath"]
        theStr = ''
        theStr += '+ MAYAVERSION:2015 assetManager any %s/mayaEnvironment\n' % (inputPath)
        theStr += 'scripts+:= scripts\n'
        theStr += '+ MAYAVERSION:2016 assetManager any %s/mayaEnvironment\n' % (inputPath)
        theStr += 'scripts+:= scripts\n'
        theStr += '+ MAYAVERSION:2017 assetManager any %s/mayaEnvironment\n' % (inputPath)
        theStr += 'scripts+:= scripts\n'
        theStr += '+ MAYAVERSION:2018 assetManager any %s/mayaEnvironment\n' % (inputPath)
        theStr += 'scripts+:= scripts\n'
        theStr += '+ MAYAVERSION:2019 assetManager any %s/mayaEnvironment\n' % (inputPath)
        theStr += 'scripts+:= scripts\n'

        return theStr

    def saveModele(self):
        versionArray = ['2014-x64','2015-x64','2016','2017','2018','2019','2020','2021']
        theData = self.makeFile()

        pathArray = [os.environ['userprofile'],'Documents','maya','%s']
        versionPath = os.sep.join(pathArray)
        for va in versionArray:
            patternPath = versionPath % va
            
            if os.path.exists(patternPath):
                modulePath = patternPath + '/modules'
                if not os.path.exists(modulePath):
                    os.makedirs(modulePath)
                moduleFile = modulePath + os.sep + 'ssToolShelf.mod'
                if os.path.exists(moduleFile):
                    os.remove(moduleFile)
                with open(moduleFile,'w') as f:
                    f.write(theData)


    def saveInstallPath(self):
        pathArray = [os.environ["ssToolShelfPath"],'FgToolshelf','mayaEnvironment','scripts','installPath_ssToolShelf.json']
        #print 'pathArray',pathArray
        ssPath = '/'.join(pathArray)
        #print 'ssPath',ssPath
        if os.path.exists(ssPath):
            os.remove(ssPath)
        jsonDump = json.dumps(os.environ["ssToolShelfPath"])
        file_object = open(ssPath, 'w')
        file_object.write(jsonDump)
        file_object.close()

    def makesureShelfIcon(self):
        logger.debug('makesureShelfIcon') 
        _label = 'ssToolShelf'
        mayaShelfName = 'Custom'
        isFound = False
        if cmds.shelfLayout(mayaShelfName, ex=1):
            if cmds.shelfLayout(mayaShelfName, q=1, ca=1):
                for each in cmds.shelfLayout(mayaShelfName, q=1, ca=1):
                    #mc.deleteUI(each)
                    logger.debug(each) 
                    getName = cmds.shelfButton(each,q=1,l=1)
                    if getName == _label:
                        isFound = True
                #else:
                #    mc.shelfLayout(mayaShelfName, p="ShelfLayout")

        if not isFound:
            logger.debug( '>>  resume icon')
            cmds.setParent(mayaShelfName)
            imagePath = os.environ['ssToolShelfPath'] + '/FgToolshelf/icons/mainIcon.png'   
            #S:\PipeProgram\figoPipe\toolShelf
            _command = 'import sys\n'
            _command += "rootPath = \"%s\"\n" % os.environ['ssToolShelfPath']
            _command += "if rootPath not in sys.path:sys.path.append(rootPath)\n"
            _command += "depositoryPath = rootPath + '/FgToolshelf/depository'\n"
            _command += "if depositoryPath not in sys.path:sys.path.append(depositoryPath)\n"
            _command += "from FgToolshelf import main\n"
            _command += "reload(main)\n"
            _command += "global ssToolshelfWindows\n"
            _command += "try:\n"
            _command += "    ssToolshelfWindows.close()\n"
            _command += "    ssToolshelfWindows = None\n"
            _command += "finally:\n"
            _command += "    ssToolshelfWindows = main.main()\n"
            
            cmds.shelfButton(width=16, height=16, image=imagePath, l=_label, command=_command, dcc=_command, imageOverlayLabel='STool', olb=(0.3,0.3,0.3,0.6), olc=(0.9,0.8,0.8))
        else:
            logger.info('>>  icon was exists ')

        initArray = [os.environ["ssToolShelfPath"],'mayaEnvironment','site-packages']
        initPath = '/'.join(initArray)
        sys.path.append(initPath)

    def main(self):
        self.makesureShelfIcon()
        self.saveModele()
        self.saveInstallPath()

if __name__ == '__main__':
    initShelfClass = initSSToolShelf()
    initShelfClass.main()
    #initShelfClass.saveModele()
